<?php

require_once __DIR__.'/AppKernel.php';

use FOS\HttpCacheBundle\SymfonyCache\EventDispatchingHttpCache;

class AppCache extends EventDispatchingHttpCache
{
    public function getOptions()
    {
        return array(
            'fos_default_subscribers' => self::SUBSCRIBER_PURGE,
        );
    }
}
