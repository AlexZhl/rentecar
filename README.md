RENTeCAR
========


How to start
-------------------

- create your own 'build.properties' file from 'build.properties.dist' template
- run "php phing.phar" command
- build assets (dev) "grunt build"
- build assets (prod) "grunt dist"
- load fixtures "php app/console doctrine:fixtures:load"