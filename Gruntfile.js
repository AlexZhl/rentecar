// Generated on 2016-01-05 using
// generator-webapp 1.1.0
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// If you want to recursively match all subfolders, use:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Automatically load required grunt tasks
  require('jit-grunt')(grunt, {
    useminPrepare: 'grunt-usemin',
    sprite: 'grunt-spritesmith',
    foo: '@abc/grunt-foo'
  });

  grunt.loadNpmTasks('grunt-combine-media-queries');
  grunt.loadNpmTasks('grunt-bower-task');
  grunt.loadNpmTasks('grunt-bower-concat');

  // Configurable paths
  var config = {
    app: 'app/Resources/HTML',
    dist: 'web',
    importResources: 'app/Resources/import'
    //add dist rename ability
  };

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Project settings
    config: config,

    // Watches files for changes and runs tasks based on the changed files
    watch: {
      options: {
        livereload: true
      },
      bower: {
        files: ['bower.json'],
        tasks: ['wiredep']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      sass: {
        files: ['<%= config.app %>/styles/{,*/}*.scss'],
        tasks: ['sass', 'postcss', 'cmq']
      },
      styles: {
        files: ['<%= config.app %>/styles/{,*/}*.css'],
        tasks: ['newer:copy:styles', 'postcss']
      },
      html: {
        files: ['<%= config.app %>/templates/{,*/}*.html'],
        tasks: ['processhtml:app']
      }
    },

    browserSync: {
      options: {
        notify: false,
        background: true,
        watchOptions: {
          ignored: ''
        }
      },
      livereload: {
        options: {
          files: [
            '<%= config.app %>/{,*/}*.html',
            '.tmp/styles/{,*/}*.css',
            '<%= config.app %>/images/{,*/}*',
            '.tmp/scripts/{,*/}*.js'
          ],
          port: 9001,
          server: {
            baseDir: ['.tmp', config.app],
            routes: {
              '/libs': './libs'
            },
            index: 'index.html'
          }
        }
      },
      dist: {
        options: {
          background: false,
          server: '<%= config.dist %>'
        }
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= config.dist %>/scripts/*',
            '<%= config.dist %>/js/*',
            '<%= config.dist %>/styles/*',
            '<%= config.dist %>/images/*',
            '<%= config.dist %>/fonts/*',
            '<%= config.dist %>/*.html',
            '<%= config.dist %>/favicon/*'
          ]
        }]
      },
      server: '.tmp'
    },

    // Make sure code styles are up to par and there are no obvious mistakes
    eslint: {
      target: [
        'Gruntfile.js',
        '<%= config.app %>/scripts/{,*/}*.js',
        '!<%= config.app %>/scripts/vendor/*',
        'test/spec/{,*/}*.js'
      ]
    },

    // Compiles scss to CSS and generates necessary files if requested
    sass: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>/styles',
          src: ['*.scss', 'base/main.scss'],
          dest: '.tmp/styles',
          ext: '.css'
        }]
      }
    },

    postcss: {
      options: {
        map: true,
        processors: [
          // Add vendor prefixed styles
          require('autoprefixer')({
            browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']
          })
        ]
      },
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>/styles/',
          src: '{,*/}*.css',
          dest: '.tmp/styles/'
        }]
      },
      app: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>/styles/',
          src: '{,*/}*.css',
          dest: '<%= config.app %>/styles/'
        }]
      }
    },

    // Automatically inject Bower components into the HTML file
    wiredep: {
      app: {
        src: ['<%= config.app %>/*.html'],
        exclude: ['bootstrap.js'],
        ignorePath: /^(\.\.\/)*\.\./
      },
      scss: {
        src: ['<%= config.app %>/styles/{,*/}*.scss'],
        ignorePath: /^(\.\.\/)+/
      }
    },

    bower: {
      install: {
        options: {
          targetDir: config.app + '/libs',
          layout: 'byComponent'
        }
      },
      dist: {
        options: {
          targetDir: config.dist + '/libs',
          layout: 'byComponent'
        }
      }
    },

    bower_concat: {
      all: {
        dest: {
          'js': config.dist + '/scripts/lib.js',
          'css': config.dist + '/styles/lib.css'
        },
        mainFiles: {
          'bootstrap': ['dist/js/bootstrap.js', 'dist/css/bootstrap.min.css'],
          'slick-carousel': ['slick/slick.js']
        }
      }
    },

    // Renames files for browser caching purposes
    filerev: {
      dist: {
        src: [
          '<%= config.dist %>/scripts/{,*/}*.js',
          '<%= config.dist %>/styles/{,*/}*.css',
          '<%= config.dist %>/images/{,*/}*.*',
          '<%= config.dist %>/fonts/{,*/}*.*',
          '<%= config.dist %>/*.{ico,png}',
          '<%= config.dist %>/favicon/{,*/}*.*'
        ]
      }
    },


    // Reads HTML for usemin blocks to enable smart builds that automatically
    // concat, minify and revision files. Creates configurations in memory so
    // additional tasks can operate on them
    useminPrepare: {
      options: {
        dest: '<%= config.dist %>'
      },
      html: '<%= config.app %>/*.html'
    },

    // Performs rewrites based on rev and the useminPrepare configuration
    usemin: {
      options: {
        assetsDirs: [
          '<%= config.dist %>',
          '<%= config.dist %>/images',
          '<%= config.dist %>/styles'
        ]
      },
      html: ['<%= config.dist %>/*.html'],
      css: ['<%= config.dist %>/styles/{,*/}*.css']
    },

    // The following *-min tasks produce minified files in the dist folder
    imagemin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>/images',
          src: '{,**/*}*.{gif,jpeg,jpg,png}',
          dest: '<%= config.dist %>/images'
        }]
      }
    },

    svgmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>/images',
          src: '{,**/*}*.svg',
          dest: '<%= config.dist %>/images'
        }]
      }
    },

    htmlmin: {
      dist: {
        options: {
          collapseBooleanAttributes: true,
          collapseWhitespace: true,
          conservativeCollapse: true,
          removeAttributeQuotes: true,
          removeCommentsFromCDATA: true,
          removeEmptyAttributes: true,
          removeOptionalTags: true,
          // true would impact styles with attribute selectors
          removeRedundantAttributes: false,
          useShortDoctype: true
        },
        files: [{
          expand: true,
          cwd: '<%= config.dist %>',
          src: '{,*/}*.html',
          dest: '<%= config.dist %>'
        }]
      }
    },

    // By default, your `*.html`'s <!-- Usemin block --> will take care
    // of minification. These next options are pre-configured if you do not
    // wish to use the Usemin blocks.
     cssmin: {
       dist: {
         files: [{
           expand: true,
           cwd: '.tmp/styles',
           src: '{,*/}*.css',
           dest: '<%= config.dist %>/styles'
         }]
       }
     },
     concat: {
       dist: {}
     },

    // Copies remaining files to places other tasks can use
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= config.app %>',
          dest: '<%= config.dist %>',
          src: [
            '*.{ico,png,txt}',
            'images/{,*/}*.*',
            'fonts/{,*/}*.*',
            'scripts/{,*/}*.js',
            'libs/{,*/}*.*',
            'favicon/{,*/}*.*'
          ]
        }]
      },
      debug: {
        files: [{
          expand: true,
          dot: true,
          cwd: '.tmp/concat',
          dest: '<%= config.dist %>',
          src: [
            '{,*/}*.*'
          ]
        }]
      },
      html: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= config.app %>',
          dest: '<%= config.dist %>',
          src: [
            '{,*/}*.html',
          ]
        }]
      },
      importImages: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= config.importResources %>/images',
          dest: '<%= config.dist %>/images',
          src: [
            'PostBodyImages/*.jpg',
          ]
        }]
      },
      importPDF: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= config.importResources %>',
          dest: '<%= config.dist %>/uploads',
          src: [
            '{,*/}*.pdf',
          ]
        }]
      }
    },
    cmq: {
      options: {
        log: true
      },
      dynamic: {
        expand: true,
        cwd: '.tmp/styles',
        src: [
          '{,*/}*.*'
        ],
        dest: '.tmp/styles/'
      }
    },

    // Run some tasks in parallel to speed up build process
    concurrent: {
      server: [
        'sass:dist',
        'cmq'
      ],
      dist: [
        'sass:dist',
        'svgmin',
        'cmq'
      ]
    },

    processhtml: {
      app:{
        options: {
          process: true
        },
        files: [
          {
            expand: true,
            cwd: '<%= config.app %>/templates/pages/',
            src: ['{,*/}*.html'],
            dest: '<%= config.app %>',
            ext: '.html'
          }
        ]
      },
      dist:{
        options: {
          process: true
        },
        files: [
          {
            expand: true,
            cwd: '<%= config.app %>/templates/pages/',
            src: ['{,*/}*.html'],
            dest: '<%= config.dist %>',
            ext: '.html'
          }
        ]
      }
    }
  });


  grunt.registerTask('serve', 'start the server and preview your app', function (target) {

    if (target === 'dist') {
      return grunt.task.run(['build', 'browserSync:dist','smq']);
    }

    grunt.task.run([
      'clean:server',
      'processhtml:app',
      'wiredep',
      'concurrent:server',
      'cmq',
      'postcss:dist',
      'browserSync:livereload',
      'watch'

    ]);
  });

  grunt.registerTask('server', function (target) {
    grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
    grunt.task.run([target ? ('serve:' + target) : 'serve']);
  });

  grunt.registerTask('build', [
    'clean:dist',
    'bower',
    'processhtml:dist',
    'postcss',
    'concurrent:dist',
    'cssmin',
    'copy:dist'
  ]);

  grunt.registerTask('html', [
    'processhtml:app'
  ]);

  grunt.registerTask('dist', [
    'clean:dist',
    'bower:dist',
    'bower_concat',
    'postcss',
    'concurrent:dist',
    'cssmin',
    'copy:dist',
    'copy:fonts',
    'copy:importImages',
    'copy:importPDF'
  ]);
};
