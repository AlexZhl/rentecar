<?php

namespace Application\Sonata\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Class SettingsAdmin.
 */
class SettingsAdmin extends Admin
{
    /**
     * @var string
     */
    protected $baseRouteName = 'sonata_setting';

    /**
     * @var string
     */
    protected $baseRoutePattern = '/';

    /**
     * @var string
     */
    protected $translationDomain = 'SettingsAdmin';

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clear();
        $collection->add('update', '/settings');
        $collection->add('list', '/settings', array(
                '_controller' => 'Sylius\Bundle\SettingsBundle\Controller\SettingsController::updateAction',
                'namespace' => 'default',
            ));
    }
}
