<?php

namespace Application\Sonata\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Doctrine\Common\Util\ClassUtils;

/**
 * Class BaseAdmin.
 */
class BaseAdmin extends Admin
{
    /**
     * {@inheritdoc}
     */
    public function toString($object)
    {
        if (!is_object($object)) {
            return '';
        }

        if (method_exists($object, '__toString') && null !== $object->__toString()) {
            return (string) $object;
        }

        $breadcrumbNew = $this->trans('breadcrumb.new');

        if ('breadcrumb.new' === $breadcrumbNew) {
            return sprintf('%s:%s', ClassUtils::getClass($object), spl_object_hash($object));
        }

        return $breadcrumbNew;
    }

    /**
     * {@inheritdoc}
     */
    public function getRequest()
    {
        if (!$this->request) {
            $this->request = $this->getConfigurationPool()->getContainer()->get('Request');
        }

        return $this->request;
    }
}
