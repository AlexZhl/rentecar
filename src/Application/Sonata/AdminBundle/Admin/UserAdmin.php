<?php

namespace Application\Sonata\AdminBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\UserBundle\Admin\Model\UserAdmin as SonataUserAdmin;
use Application\Sonata\UserBundle\Entity\Admin as User;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Corporation\CoreBundle\Validator\Constraints\Password;
use Corporation\CoreBundle\Validator\Constraints\PasswordValidator;
use Corporation\CoreBundle\Entity\Password as PasswordStorage;
use Sonata\CoreBundle\Validator\ErrorElement;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class UserAdmin extends SonataUserAdmin
{
    protected $baseRouteName = 'administrator';
    protected $baseRoutePattern = 'administrator';
    protected $translationDomain = 'UserAdmin';

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('show');
        $collection->add('generateCode', $this->getRouterIdParameter() . '/generateCode');
    }

    public function getBatchActions()
    {
        // retrieve the default batch actions (currently only delete)
        $actions = parent::getBatchActions();

        if (
            $this->hasRoute('edit') && $this->isGranted('EDIT') &&
            $this->hasRoute('delete') && $this->isGranted('DELETE')
        ) {
            $actions['generateCode'] = array(
                'label' => 'btn_generate_new_secret',
                'translation_domain' => 'SonataAdminBundle',
                'ask_confirmation' => true,
            );
        }

        return $actions;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $user = $this->getUser();
        $isReadOnly = !($this->id($this->getSubject()) == null || ($user->getId() != $this->getSubject()->getId()));

        $fieldAttrs = array();

        if ($isReadOnly) {
            $fieldAttrs += array('readonly' => 'readonly');
        }

        $formMapper
            ->with('Profile')
            ->add('firstname', null, array(
                'label' => 'firstname_label',
                'attr' => array('maxlength' => 50) + $fieldAttrs,
                'required' => true,
            ))
            ->add('lastname', null, array(
                'label' => 'lastname_label',
                'attr' => array('maxlength' => 50) + $fieldAttrs,
                'required' => true,
            ))
            ->add('email', null, array(
                'attr' => $fieldAttrs,
                'label' => 'email_label',
            ));
        if (!$this->id($this->getSubject())) {
            $formMapper->add('role', ChoiceType::class, array(
                'choices' => array(
                    'ROLE_ADMIN' => 'Admin',
                    'ROLE_SUPER_ADMIN' => 'Global admin'
                ),
                'multiple' => false,
                'label' => 'role_label',
            ));
        }
        $formMapper->end();

        if ($user->getId() != $this->getSubject()->getId() && $this->id($this->getSubject())) {
            $formMapper->add(
                'singleRole',
                'text',
                [
                    'label' => 'role_label',
                    'required' => false,
                    'attr' => [
                        'readonly' => 'readonly',
                    ],
                ]
            );
        }

        if ($this->id($this->getSubject()) != null) {
            $formMapper
                ->end()
                ->with('Security')
                ->add(
                    'twoStepVerificationCode',
                    'text',
                    array(
                        'required' => false,
                        'label' => 'shared_secret_label',
                        'attr' => array('readonly' => 'readonly'),
                    )
                )->end();

            if ($user->getId() == $this->getSubject()->getId()) {
                $formMapper->with('user_block_reset_password')
                    ->add('oldPassword', 'password', array(
                        'required' => false,
                        'label' => 'enter_old_password_label',
                        'mapped' => false,
                        'always_empty' => true,
                        'attr' => array(
                            'autocomplete' => 'off',
                        ),
                        'constraints' => [
                            new UserPassword([
                                'message' => 'Current Password is wrong',
                                'groups' => [
                                    'password',
                                ],
                            ]),
                        ],
                    ))
                    ->add('plainPassword', 'repeated', [
                        'type' => 'password',
                        'required' => false,
                        'invalid_message' => 'New Password and Confirm password don\'t match',
                        'options' => [
                            'required' => false,
                        ],
                        'first_options' => [
                            'label' => 'enter_new_password_label',
                            'translation_domain' => 'SonataUserBundle',
                        ],
                        'second_options' => [
                            'label' => 'confirm_new_password_label',
                            'translation_domain' => 'SonataUserBundle',
                        ],
                        'constraints' => [
                            new \Rollerworks\Bundle\PasswordStrengthBundle\Validator\Constraints\PasswordRequirements([
                                'minLength' => 8,
//                                'tooShortMessage' => 'password.too.simple',
                                'requireLetters' => true,
//                                'missingLettersMessage' => 'password.too.simple',
                                'requireCaseDiff' => true,
//                                'requireCaseDiffMessage' => 'password.too.simple',
                                'requireSpecialCharacter' => false,
//                                'missingSpecialCharacterMessage' => 'password.too.simple',
                                'groups' => array(
                                    'password',
                                ),
                            ]),
                            new Length(array('max' => 72, 'groups' => array('password'))),
                            new \Corporation\CoreBundle\Validator\Constraints\Password([
                                    'groups' => array('password'),
                                    'email' => $this->getUserEmail(),
                                ]
                            )
                        ],
                    ])
                    ->end();
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, ['label' => 'ID'])
            ->addIdentifier('firstname', null, ['label' => 'list.label_firstname'])
            ->addIdentifier('lastname', null, ['label' => 'list.label_lastname'])
            ->add('email')
            ->add('singleRole')
            ->add('createdAt');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper
            ->add('id')
            ->add('firstname')
            ->add('lastname')
            ->add('email');
    }

    public function prePersist($object)
    {
        $container = $this->getContainer();
        $tokenGenerator = $this->getContainer()->get('fos_user.util.token_generator');
        $password = substr($tokenGenerator->generateToken(), 0, 12);

        /* @var $object User */
        $object->setPlainPassword($password);
        $object->setEnabled(false);
        $object->setUsername($object->getEmail());
        if ($container->has('sonata.user.google.authenticator.provider')) {
            $object->setTwoStepVerificationCode(
                $container->get('sonata.user.google.authenticator.provider')->generateSecret()
            );
        }
        $object->setConfirmationToken($container->get('fos_user.util.token_generator')->generateToken());
        $object->setPasswordRequestedAt(new \DateTime());
        $object->setUsernameCanonical($object->getEmail());
    }

    /**
     * @param \Application\Sonata\UserBundle\Entity\Admin $object
     */
    public function postPersist($object)
    {
        $container = $this->getContainer();
        $mailManager = $container->get('corporation_core.mailer_manager');
        $confirmationToken = $object->getConfirmationToken();
        $sharedSecret = $object->getTwoStepVerificationCode();

        $params = [];
        $params['subject'] = $this->trans('admin_account_created.subject', array(), 'email');
        $params['email'] = $object->getEmail();
        $params['body'] = $container->get('templating')->render(
            '@CorporationCore/Email/admin_account_created.html.twig',
            [
                'confirmationToken' => $confirmationToken,
                'user' => $object,
            ]
        );

        $mailManager->sendMail($params);
        $params['subject'] = $this->trans('admin_account_created_second.subject', array(), 'email');
        $params['body'] = $container->get('templating')->render(
            '@CorporationCore/Email/admin_account_created_second.html.twig',
            [
                'sharedSecret' => $sharedSecret,
                'user' => $object,
            ]
        );

        $mailManager->sendMail($params);
    }

    public function preUpdate($user)
    {
        parent::preUpdate($user);

        $em = $this->getEntityManager();
        $originalObject = $em->getUnitOfWork()->getOriginalEntityData($user);

        if ($originalObject['password'] !== $user->getPassword()) {
            $this->addIntoStorage($user, $em);
        }
        $this->deleteOldestPassword($user, $em);
    }

    private function addIntoStorage($user, $em)
    {
        $pass = new PasswordStorage();
        $pass->setUser($user);
        $pass->setPassword($user->getPassword());
        $em->persist($pass);
        $em->flush();
    }

    private function deleteOldestPassword($user, $em)
    {
        $repository = $this->getPasswordRepository($em);
        $storedPass = $repository->findByUser($user);
        if (!empty($storedPass) && count($storedPass) > 5) {
            $em->remove($storedPass[0]);
            $em->flush();
        }
    }

    private function getPasswordRepository($em)
    {
        return $em->getRepository('Corporation\CoreBundle\Entity\Password');
    }

    public function getFormBuilder()
    {
        $this->formOptions['data_class'] = $this->getClass();

        $this->formOptions['validation_groups'] = function (FormInterface $form) {
            $defaultValidationGroup = ((!$this->getSubject() || is_null($this->getSubject()->getId())) ? 'CorporationRegistration' : 'CorporationProfile');
            if ($form->has('plainPassword') && $form->get('plainPassword')->getData()) {
                return array($defaultValidationGroup, 'password');
            }

            return array($defaultValidationGroup);
        };

        $formBuilder = $this->getFormContractor()->getFormBuilder(
            $this->getUniqid(),
            $this->formOptions
        );

        $this->defineFormBuilder($formBuilder);

        return $formBuilder;
    }

    private function getUser()
    {
        return $this->getContainer()->get('security.token_storage')->getToken()->getUser();
    }

    private function getUserEmail()
    {
        return $this->getUser()->getEmail();
    }

    private function getEntityManager()
    {
        return $this->getModelManager()->getEntityManager($this->getClass());
    }

    private function getContainer()
    {
        return $this->getConfigurationPool()->getContainer();
    }
}
