<?php

namespace Application\Sonata\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Class SettingsAdmin.
 */
class ServiceAdmin extends Admin
{
    /**
     * @var string
     */
    protected $baseRouteName = 'service_admin';

    /**
     * @var string
     */
    protected $baseRoutePattern = '/';

    /**
     * @var string
     */
    protected $translationDomain = 'ServiceAdmin';

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clear();
        //$collection->add('update', '/settings');
        $collection->add('clearcache', '/service/clear-cache', array(
                '_controller' => 'Application\Sonata\AdminBundle\Controller\ServiceAdminController::clearCacheAction',
                'namespace' => 'default',
        ));

        $collection->add('service', '/service', array(
            '_controller' => 'Application\Sonata\AdminBundle\Controller\ServiceAdminController::indexAction',
            'namespace' => 'default',
        ));

        $collection->add('getlog', '/service/log', array(
            '_controller' => 'Application\Sonata\AdminBundle\Controller\ServiceAdminController::getLogAction',
            'namespace' => 'default',
        ));

    }
}
