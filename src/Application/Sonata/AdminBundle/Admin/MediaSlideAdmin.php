<?php

namespace Application\Sonata\AdminBundle\Admin;

use Application\Sonata\MediaBundle\Entity\Media;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Sonata\MediaBundle\Admin\ORM\MediaAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\MediaBundle\Form\DataTransformer\ProviderDataTransformer;
use Sonata\AdminBundle\Datagrid\ListMapper;

class MediaSlideAdmin extends MediaAdmin
{
    const CAROUSEL_CATEGORY = 'carousel';

    protected $baseRouteName = 'media_slider';
    protected $baseRoutePattern = 'media-slider';
    protected $translationDomain = 'MediaSlideAdmin';

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getPersistentParameters()
    {
        $parameters = array();

        foreach ($this->getExtensions() as $extension) {
            $params = $extension->getPersistentParameters($this);

            if (!is_array($params)) {
                throw new \RuntimeException(sprintf('The %s::getPersistentParameters must return an array', get_class($extension)));
            }

            $parameters = array_merge($parameters, $params);
        }

        return $parameters;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('description')
            ->add('enabled', null, ['editable' => true])
            ->add('size')
        ;
    }

    public function createQuery($context = 'list')
    {
        /** @var ProxyQuery $query */
        $query = parent::createQuery($context);
        $query->entityJoin([['fieldName' => 'category']]);

        $query->andWhere(
            $query->expr()->eq('s_category.name', ':name')
        );
        $query->setParameter('name', self::CAROUSEL_CATEGORY);

        return $query;
    }

    public function prePersist($object)
    {
        $container = $this->getConfigurationPool()->getContainer();
        $category = $container->get('doctrine')->getRepository('ApplicationSonataClassificationBundle:Category')->findOneBy(['name' => self::CAROUSEL_CATEGORY]);

        /* @var $object Media */
        $object->setCategory($category);
        $object->setContext(self::CAROUSEL_CATEGORY);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $media = $this->getSubject();

        if (!$media) {
            $media = $this->getNewInstance();
        }

        if (!$media || !$media->getProviderName()) {
            return;
        }

        $formMapper->add('providerName', 'hidden');

        $formMapper->getFormBuilder()->addModelTransformer(new ProviderDataTransformer($this->pool, $this->getClass()), true);

        $provider = $this->pool->getProvider($media->getProviderName());

        if (in_array($provider->getName(), ['sonata.media.provider.youtube', 'sonata.media.provider.vimeo'])) {
            if (!$media->getId()) {
                $label = $provider->getName() == 'sonata.media.provider.youtube' ? 'Youtube URL' : 'Vimeo URL';
                $formMapper->add('binaryContent', 'text', ['label' => $label]);
            }
        } else {
            if (!$media->getId()) {
                $formMapper->add('binaryContent', 'file', ['label' => 'Image']);
            }
        }

        if ($media->getId()) {
            $formMapper->add('name', null, ['label' => 'Name', 'required' => false]);
            $formMapper->add('description', null, ['required' => false, 'label' => 'Description']);
        }

        $formMapper->add('enabled');
    }
}
