<?php

namespace Application\Sonata\AdminBundle\Controller;

use Sylius\Bundle\SettingsBundle\Controller\SettingsController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SettingsAdminController.
 */
class SettingsAdminController extends SettingsController
{
    /**
     * @param Request $request
     * @param string  $namespace
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function updateAction(Request $request, $namespace = 'default')
    {
        $manager = $this->getSettingsManager();
        $settings = $manager->loadSettings($namespace);

        $form = $this
            ->getSettingsFormFactory()
            ->create($namespace)
        ;

        $form->setData($settings);
        $form->handleRequest($request);
        if ($request->isMethod('POST') && $form->isValid()) {
            $manager->saveSettings($namespace, $form->getData());
            $message = $this->getTranslator()->trans('updated_successful', array(), 'SettingsAdmin');
            $this->get('session')->getFlashBag()->add('sonata_flash_success', $message);

            return $this->redirect($request->headers->get('referer'));
        }

        $admin_pool = $this->get('sonata.admin.pool');

        return $this->render('ApplicationSonataAdminBundle:Admin:settings.html.twig', array(
            'settings' => $settings,
            'form' => $form->createView(),
            'admin_pool' => $admin_pool,
        ));
    }
}
