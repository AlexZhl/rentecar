<?php

namespace Application\Sonata\AdminBundle\Filter;

use Sonata\DoctrineORMAdminBundle\Filter\DateTimeRangeFilter as BaseDateTimeRangeFilter;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\AdminBundle\Form\Type\Filter\DateRangeType;

class DateTimeRangeFilter extends BaseDateTimeRangeFilter
{
    /**
     * {@inheritdoc}
     */
    public function filter(ProxyQueryInterface $queryBuilder, $alias, $field, $data)
    {
        // check data sanity
        if (!$data || !is_array($data) || !array_key_exists('value', $data)) {
            return;
        }

        if ($this->range) {
            //            // additional data check for ranged items
//            if (!array_key_exists('start', $data['value']) && !array_key_exists('end', $data['value'])) {
//                return;
//            }

            if (empty($data['value']['start']) && empty($data['value']['end'])) {
                return;
            }

            if ($data['type'] == DateRangeType::TYPE_NOT_BETWEEN &&
                (empty($data['value']['start']) || empty($data['value']['end']))) {
                return;
            }

            // date filter should filter records for the whole days
            if ($this->time === false) {
                if ($data['value']['start'] instanceof \DateTime) {
                    $data['value']['start']->setTime(0, 0, 0);
                }
                if ($data['value']['end'] instanceof \DateTime) {
                    $data['value']['end']->setTime(23, 59, 59);
                }
            }

            // transform types
            if ($this->getOption('input_type') === 'timestamp') {
                $data['value']['start'] = $data['value']['start'] instanceof \DateTime ? $data['value']['start']->getTimestamp() : 0;
                $data['value']['end'] = $data['value']['end'] instanceof \DateTime ? $data['value']['end']->getTimestamp() : 0;
            }

            // default type for range filter
            $data['type'] = !isset($data['type']) || !is_numeric($data['type']) ?  DateRangeType::TYPE_BETWEEN : $data['type'];

            $startDateParameterName = $this->getNewParameterName($queryBuilder);
            $endDateParameterName = $this->getNewParameterName($queryBuilder);

            if ($data['type'] == DateRangeType::TYPE_NOT_BETWEEN) {
                $this->applyWhere($queryBuilder, sprintf('%s.%s < :%s OR %s.%s > :%s', $alias, $field, $startDateParameterName, $alias, $field, $endDateParameterName));
            } else {
                if ($data['value']['start']) {
                    $this->applyWhere($queryBuilder, sprintf('%s.%s %s :%s', $alias, $field, '>=', $startDateParameterName));
                    $queryBuilder->setParameter($startDateParameterName,  $data['value']['start']);
                }
                if ($data['value']['end']) {
                    $this->applyWhere($queryBuilder, sprintf('%s.%s %s :%s', $alias, $field, '<=', $endDateParameterName));
                    $queryBuilder->setParameter($endDateParameterName,  $data['value']['end']);
                }
            }
        }
    }
}
