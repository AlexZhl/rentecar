<?php

namespace Application\Sonata\AdminBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

/**
 * Class ExtendDefaultPageServiceCompilerPass.
 */
class OverrideRangeFilterCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if ($container->hasDefinition('sonata.admin.orm.filter.type.datetime_range')) {
            $container->getDefinition('sonata.admin.orm.filter.type.datetime_range')
                 ->setClass('Application\Sonata\AdminBundle\Filter\DateTimeRangeFilter')
             ;
        }
    }
}
