<?php

namespace Application\Sonata\AdminBundle\Exporter\Source;

use Exporter\Source\DoctrineORMQuerySourceIterator;

class SonataDoctrineORMQuerySourceIterator extends DoctrineORMQuerySourceIterator
{
    /**
     * @param $value
     *
     * @return null|string
     */
    protected function getValue($value)
    {
        if (is_array($value) || $value instanceof \Traversable) {
            $value = null;
        } elseif ($value instanceof \DateTime) {
            $value = $value->format($this->dateTimeFormat);
        } elseif (is_object($value)) {
            $value = (string) $value;
        } elseif (is_bool($value)) {
            $value = $value ? 'Yes' : 'No';
        }

        return $value;
    }
}
