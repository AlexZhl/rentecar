<?php

namespace Application\Sonata\AdminBundle\EventListener;

use Symfony\Cmf\Component\Routing\ChainRouter;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class AuthenticationHandler implements AuthenticationSuccessHandlerInterface
{
    /** @var  ChainRouter */
    protected $router;

    public function __construct(ChainRouter $router)
    {
        $this->router = $router;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
         return new RedirectResponse($this->router->generate('admin_corporation_core_customer_list'));
    }
}
