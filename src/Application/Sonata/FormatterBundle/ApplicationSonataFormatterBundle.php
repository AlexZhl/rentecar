<?php

namespace Application\Sonata\FormatterBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ApplicationSonataFormatterBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'SonataFormatterBundle';
    }
}
