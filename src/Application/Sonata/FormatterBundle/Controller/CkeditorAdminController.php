<?php

namespace Application\Sonata\FormatterBundle\Controller;

use Sonata\FormatterBundle\Controller\CkeditorAdminController as BaseController;

class CkeditorAdminController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function browserAction()
    {
        // FIXME: this is a bit hacky but the original implementation doesn't work well.
        // The problem is that when we pass
        // filebrowserImageBrowseRouteParameters: { 'filter[context][value]': 'wysiwyg' }
        // from the config the default context category isn't used
        // so we eithe have to add a hardcoded category id to filebrowserImageBrowseRouteParameters
        // or use this workaround
        if (!$this->getRequest()->get('category') &&
            $category = $this->admin->getPersistentParameter('category')) {
            $this->getRequest()->request->set('category', $category);
        }
        return parent::browserAction();
    }
}
