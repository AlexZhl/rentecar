<?php

namespace Application\Sonata\FormatterBundle\Block;

use Sonata\FormatterBundle\Block\FormatterBlockService as BaseFormatterBlockService;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\BlockBundle\Model\BlockInterface;
use Symfony\Component\Form\FormBuilderInterface;

class FormatterBlockService extends BaseFormatterBlockService
{
    /**
     * {@inheritdoc}
     */
    public function buildEditForm(FormMapper $formMapper, BlockInterface $block)
    {
        $formMapper->add('settings', 'sonata_type_immutable_array', array(
            'keys' => array(
                array('content', 'sonata_simple_formatter_type', function (FormBuilderInterface $formBuilder) {
                    return array(
                        'label' => 'form.label_content',
                        'format' => 'richhtml',
                        'ckeditor_context' => 'default',
                    );
                }),
            ),
            'translation_domain' => 'SonataFormatterBundle',
        ));
    }
}
