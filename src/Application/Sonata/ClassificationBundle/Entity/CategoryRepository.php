<?php

namespace Application\Sonata\ClassificationBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Application\Sonata\ClassificationBundle\Entity\Context;
use Application\Sonata\NewsBundle\Entity\Post;

class CategoryRepository extends EntityRepository
{

    public function getCategoriesByContext($context)
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('c')
            ->from('ApplicationSonataClassificationBundle:Category', 'c')
            ->where('c.context = :context')
            ->setParameter('context', $context)
            ->indexBy('c', 'c.slug')
            ->getQuery()
        ;
        return $query->getResult();
    }

    public function getCategoriesForPeople()
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('c')
            ->from(Category::class, 'c')
            ->innerJoin('c.posts', 'p')
            ->where('c.context = :context')
            ->andWhere('p.enabled = :enabled')
            ->setParameters(array('context' => Context::PEOPLE, 'enabled' => Post::ENABLED))
            ->indexBy('c', 'c.slug')
            ->getQuery()
        ;
        return $query->getResult();
    }
}
