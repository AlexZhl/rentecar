<?php

namespace Application\Sonata\ClassificationBundle\Entity;

use Doctrine\ORM\EntityRepository;

class TagRepository extends EntityRepository
{

    public function getTagsByContext($context)
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('t')
            ->from('ApplicationSonataClassificationBundle:Tag', 't')
            ->where('t.context = :context')
            ->setParameter('context', $context)
            ->indexBy('t', 't.slug')
            ->getQuery()
        ;
        return $query->getResult();
    }

}
