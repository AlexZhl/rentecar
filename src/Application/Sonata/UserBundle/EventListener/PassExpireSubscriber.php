<?php

namespace Application\Sonata\UserBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Application\Sonata\UserBundle\Entity\Admin;

class PassExpireSubscriber implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return array(
            'prePersist'
        );
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $this->index($args);
    }

    public function index(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof Admin) {
            $date = new \DateTime('now');
            $dateExpires = $date->add(new \DateInterval('P90D'));
            $entity->setCredentialsExpireAt($dateExpires);
        }
    }
}