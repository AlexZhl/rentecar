<?php

namespace Application\Sonata\UserBundle\EventListener;

use FOS\UserBundle\Model\UserManager;
use Application\Sonata\UserBundle\Entity\Admin;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\AuthenticationEvents;

//use FOS\UserBundle\Event\GetResponseUserEvent;
//use FOS\UserBundle\Event\FormEvent;

use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Symfony\Component\Translation\Translator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class UserAuthenticationListener implements EventSubscriberInterface
{
    private $router;
    private $translator;
    private $userManager;
    private $failedAttemptsCount;
    private $userInactivityTime;
    private $container;

    public function __construct(
        UrlGeneratorInterface $router,
        Translator $translator,
        UserManager $userManager,
        $userInactivityTime,
        $failedAttemptsCount,
        $container
    )
    {
        $this->router = $router;
        $this->translator = $translator;
        $this->userManager = $userManager;
        $this->userInactivityTime = $userInactivityTime;
        $this->failedAttemptsCount = $failedAttemptsCount;
        $this->container= $container;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            AuthenticationEvents::AUTHENTICATION_FAILURE => 'onAuthenticationFailure',

        );
    }


    public function onAuthenticationFailure(AuthenticationFailureEvent $event)
    {
        $userName = $event->getAuthenticationToken()->getUser();
        $user = $this->userManager->findUserByUsername($userName);

        if ($user instanceof Admin) {
            $userLoginFailuresCount = ($user->getLoginFailuresCount() == $this->failedAttemptsCount)
                ? 0
                : $user->getLoginFailuresCount() + 1
            ;

            $user->setLoginFailuresCount($userLoginFailuresCount);

            if ($userLoginFailuresCount >= $this->failedAttemptsCount) {
                $lockedUntil = new \DateTime('now');
                $lockedUntil->modify('+' . $this->userInactivityTime . ' minutes');

                $user->setLocked(true);
                $user->setLockedUntil($lockedUntil);
            }

            $this->userManager->updateUser($user);
        }
    }


    public function resetUserLoginFailuresCount($user)
    {
        if ($user instanceof Admin) {
            $user->setLoginFailuresCount(0);
            $user->setLockedUntil(null);
            $user->setLocked(false);
        }
    }

    public function setResponse($event)
    {
        $url = $this->router->generate('corporation_index');
        $event->setResponse(new RedirectResponse($url));
    }
}