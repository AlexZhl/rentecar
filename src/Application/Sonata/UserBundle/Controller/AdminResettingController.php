<?php

namespace Application\Sonata\UserBundle\Controller;

use Sonata\UserBundle\Controller\AdminResettingController as SonataAdminResettingController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Corporation\CoreBundle\Validator\Constraints\Password;
use Symfony\Component\Validator\Constraints\Email as EmailConstraint;
use Symfony\Component\Validator\Constraints\Email;
use Corporation\CoreBundle\Entity\Password as PasswordStorage;

class AdminResettingController extends SonataAdminResettingController
{
    /**
     * {@inheritdoc}
     */
    public function resetAction($token)
    {
        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return new RedirectResponse($this->container->get('router')->generate('sonata_admin_dashboard'));
        }

        $user = $this->container->get('fos_user.user_manager')->findUserByConfirmationToken($token);

        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with "confirmation token" does not exist for value "%s"', $token));
        }

        if (!$user->isPasswordRequestNonExpired($this->container->getParameter('fos_user.resetting.token_ttl'))) {
            return new RedirectResponse($this->container->get('router')->generate('sonata_user_admin_resetting_request'));
        }

        $email = $user->getEmail();

        $validator = $this->container->get('corporation.validation.password');
        $validator->setEmail($email);

        $form = $this->container->get('fos_user.resetting.form');
        $formHandler = $this->container->get('corporation.user.resetting.form');
        $process = $formHandler->process($user);
        $passExists = false;
        $em = $this->container->get('doctrine.orm.entity_manager');

        if ($process) {
            $encoder = $this->container->get('security.password_encoder');
            $currentPasswordEncoded = $encoder->encodePassword($user, $formHandler->getNewPassword());

            $repository = $this->getPasswordRepository($em);
            $passExists =  $repository->findByPassword($currentPasswordEncoded);

            if ($passExists) {
                $passExists = ['message' => 'Sorry, the password is too simple. Please try again.'];
            }

            if (!$passExists) {
                $formHandler->onSuccess($user);
                $this->addIntoStorage($user, $em);
                $this->deleteOldestPassword($user, $em);
                $this->resetLockedAndExpired($user, $em);
                $em->flush();

                $this->setFlash('fos_user_success', 'resetting.flash.success');
                $response = new RedirectResponse($this->container->get('router')->generate('sonata_admin_dashboard'));
                $this->authenticateUser($user, $response);
                
                $newToken = $this->container->get('security.token_storage')->getToken();
                $this->container->get('request')->getSession()->set(
                    $this->container->get('sonata.user.google.authenticator.provider')->getSessionKey($newToken),
                    null
                );

                return $response;
            }

        }

        return $this->container->get('templating')->renderResponse('SonataUserBundle:Admin:Security/Resetting/reset.html.'.$this->getEngine(), array(
            'token' => $token,
            'form' => $form->createView(),
            'base_template' => $this->container->get('sonata.admin.pool')->getTemplate('layout'),
            'admin_pool' => $this->container->get('sonata.admin.pool'),
            'passExists' => $passExists
        ));
    }

    private function resetLockedAndExpired($user, $em) {
        $user->setLoginFailuresCount(false);
        $date = new \DateTime('now');
        $dateExpires =  $date->add(new \DateInterval('P90D'));
        $user->setLocked(false);
        $user->setExpiresAt($dateExpires);
        $user->setExpired(false);
        $em->persist($user);
    }

    private function addIntoStorage($user, $em) {
        $pass = new PasswordStorage();
        $pass->setUser($user);
        $pass->setPassword($user->getPassword());
        $em->persist($pass);
    }

    private function deleteOldestPassword($user, $em) {
        $repository = $this->getPasswordRepository($em);
        $storedPass =  $repository->findByUser($user);
        if (!empty($storedPass) && count($storedPass) > 5) {
            $em->remove($storedPass[0]);
        }
    }

    private function getPasswordRepository($em) {
        return $em->getRepository( 'Corporation\CoreBundle\Entity\Password' );
    }

}
