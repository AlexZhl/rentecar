<?php

namespace Application\Sonata\UserBundle\Entity;

use Sonata\UserBundle\Entity\BaseGroup as BaseGroup;

class Group extends BaseGroup
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $users;

    public function __construct($name, $roles)
    {
        parent::__construct($name, $roles);
        $this->users = new ArrayCollection();
    }

    /**
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param UserInterface $user
     *
     * @return $this
     */
    public function addUser(UserInterface $user)
    {
        $this->users->add($user);

        return $this;
    }

    /**
     * @param UserInterface $user
     *
     * @return $this
     */
    public function removeUser(UserInterface $user)
    {
        $this->users->removeElement($user);

        return $this;
    }
}
