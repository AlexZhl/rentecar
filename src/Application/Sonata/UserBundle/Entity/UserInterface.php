<?php

namespace Application\Sonata\UserBundle\Entity;

interface UserInterface extends \FOS\UserBundle\Model\UserInterface
{
    const GENDER_FEMALE = 'f';
    const GENDER_MALE = 'm';
    const GENDER_MAN = 'm'; // @deprecated
    const GENDER_UNKNOWN = 'u';

    /**
     * @return string
     */
    public function getTwoStepVerificationCode();

    /**
     * @param string $code
     *
     * @return string
     */
    public function setTwoStepVerificationCode($code);
}
