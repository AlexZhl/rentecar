<?php

namespace Application\Sonata\UserBundle\Entity;

use Sonata\UserBundle\Entity\BaseUser as BaseUser;

class Admin extends BaseUser
{
    /**
     * @var int
     */
    protected $id;


    /**
     * Get id.
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    public function getRole() {
        if (isset($this->roles[0])) {
            return $this->roles[0];
        } else {
            return null;
        }
    }

    public function setRole($role) {
        $role = strtoupper($role);
        if ($role === static::ROLE_DEFAULT) {
            return $this;
        }

        if (!in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function getSingleRole()
    {
        return $this->isSuperAdmin() ? 'Global administrator' : 'Administrator';
    }

    /**
     * Returns a string representation.
     *
     * @return string
     */
    public function __toString()
    {
        $string = $this->getFirstname().' '.$this->getLastname();
        if (!$string) {
            $string = $this->getUsername();
        }

        return $string ?: '';
    }


    protected $loginFailuresCount = 0;

    /**
     * @return int
     */
    public function getLoginFailuresCount()
    {
        return $this->loginFailuresCount;
    }

    /**
     * @param int $loginFailuresCount
     */
    public function setLoginFailuresCount($loginFailuresCount)
    {
        $this->loginFailuresCount = $loginFailuresCount;
    }

    protected $lockedUntil;

    /**
     * @return \DateTime
     */
    public function getLockedUntil()
    {
        return $this->lockedUntil;
    }

    /**
     * @param \DateTime $lockedUntil
     */
    public function setLockedUntil($lockedUntil)
    {
        $this->lockedUntil = $lockedUntil;
    }
}
