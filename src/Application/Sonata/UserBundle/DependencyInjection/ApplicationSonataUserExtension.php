<?php

namespace Application\Sonata\UserBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Sonata\EasyExtendsBundle\Mapper\DoctrineCollector;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Config\FileLocator;

class ApplicationSonataUserExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $collector = DoctrineCollector::getInstance();
        $associations = $collector->getAssociations();

        $userClass = $config['class']['user'];
        $associations[$userClass]['mapManyToMany'][0]['joinTable']['joinColumns'][0]['referencedColumnName'] = 'id';
        $associations[$userClass]['mapManyToMany'][0]['joinTable']['inverseJoinColumns'][0]['referencedColumnName'] = 'group_id';

        $collector->addAssociation($userClass, 'mapManyToMany',  $associations[$userClass]['mapManyToMany'][0]);
    }
}
