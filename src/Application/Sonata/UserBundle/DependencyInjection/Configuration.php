<?php

namespace Application\Sonata\UserBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('application_sonata_user');

        $rootNode
            ->children()
            ->arrayNode('class')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('user')->defaultValue('Application\Sonata\UserBundle\Entity\Admin')->cannotBeEmpty()->end()
            ->end()
            ->end()
            ->end();

        return $treeBuilder;
    }
}
