<?php

namespace Application\Sonata\UserBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

class OverrideFosMailerClassCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if ($container->hasDefinition('fos_user.mailer.twig_swift')) {
            $definition = $container->getDefinition('fos_user.mailer.twig_swift');
            $definition->setClass('Application\\Sonata\\UserBundle\\Mailer\\TwigSwiftMailer');
        }
    }
}