<?php

namespace Application\Sonata\UserBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

class AddORMMapperCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if ($container->hasDefinition('sonata.easy_extends.doctrine.mapper')) {
            $definition = $container->getDefinition('sonata.easy_extends.doctrine.mapper');
            $definition->setClass('Application\\Sonata\\UserBundle\\Mapper\\DoctrineORMMapper');
        }
    }
}
