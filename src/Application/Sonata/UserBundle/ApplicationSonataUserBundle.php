<?php

namespace Application\Sonata\UserBundle;

use Application\Sonata\UserBundle\DependencyInjection\Compiler\OverrideFosMailerClassCompilerPass;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Application\Sonata\UserBundle\DependencyInjection\Compiler\AddORMMapperCompilerPass;

class ApplicationSonataUserBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'SonataUserBundle';
    }

    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new AddORMMapperCompilerPass());
        $container->addCompilerPass(new OverrideFosMailerClassCompilerPass());
    }
}
