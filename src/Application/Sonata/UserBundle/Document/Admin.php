<?php

namespace Application\Sonata\UserBundle\Document;

use Sonata\UserBundle\Document\BaseUser as BaseUser;


class Admin extends BaseUser
{
    /**
     * @var int
     */
    protected $id;

    /**
     * Get id.
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }
}
