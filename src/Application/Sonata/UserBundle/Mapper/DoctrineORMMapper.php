<?php

namespace Application\Sonata\UserBundle\Mapper;

use Sonata\EasyExtendsBundle\Mapper\DoctrineORMMapper as SonataDoctrineORMMapper;

class DoctrineORMMapper extends SonataDoctrineORMMapper
{
    /**
     * {@inheritdoc}
     */
    public function addAssociation($class, $field, array $options)
    {
        if (!isset($this->associations[$class])) {
            $this->associations[$class] = array();
        }

        $optionsMerged = array();
        foreach ($options as $mapping) {
            $optionsMerged[$mapping['fieldName']] = $mapping;
        }

        $this->associations[$class][$field] = $optionsMerged;
    }
}
