<?php
namespace Application\Sonata\MediaBundle\Provider;


use Sonata\MediaBundle\Model\MediaInterface;

class ImageProvider extends \Sonata\MediaBundle\Provider\ImageProvider{
    /**
     * {@inheritdoc}
     */
   public function preUpdate(MediaInterface $media)
    {
        $media->setUpdatedAt(new \Datetime());

        if ($this->requireThumbnails()) {

            if($media->getPreviousProviderReference() !== null &&  $media->getPreviousProviderReference() !== $media->getProviderReference()){
                $providerReference = $media->getProviderReference();
                $media->setProviderReference($media->getPreviousProviderReference());
                $this->thumbnail->delete($this, $media);
                $media->setProviderReference($providerReference);
            }
        }
    }
}