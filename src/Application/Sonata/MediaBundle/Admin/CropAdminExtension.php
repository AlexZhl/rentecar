<?php

namespace Application\Sonata\MediaBundle\Admin;

use Sonata\AdminBundle\Admin\AdminExtension;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;

class CropAdminExtension extends AdminExtension
{
    public function configureRoutes(AdminInterface $admin, RouteCollection $collection)
    {
        $collection->add(
            'crop',
            $admin->getRouterIdParameter().'/crop/{format}',
            array(
                '_controller' => 'JMaitanSonataMediaCropBundle:Crop:index',
                'format' => 'reference',
            )
        );
    }
}