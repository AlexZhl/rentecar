<?php

namespace Application\Sonata\MediaBundle\Document;

use Sonata\MediaBundle\Document\BaseGallery as BaseGallery;

class Gallery extends BaseGallery
{
    /**
     * @var int
     */
    protected $id;

    /**
     * Get id.
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }
}
