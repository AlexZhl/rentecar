<?php

namespace Application\Sonata\MediaBundle\Document;

use Sonata\MediaBundle\Document\BaseMedia as BaseMedia;

class Media extends BaseMedia
{
    /**
     * @var int
     */
    protected $id;

    /**
     * Get id.
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }
}
