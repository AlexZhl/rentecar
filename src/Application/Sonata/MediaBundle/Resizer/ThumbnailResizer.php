<?php

namespace Application\Sonata\MediaBundle\Resizer;

use Imagine\Filter\Basic\Resize;
use Imagine\Image\ImagineInterface;
use Imagine\Image\Box;
use Gaufrette\File;
use Sonata\MediaBundle\Model\MediaInterface;
use Sonata\MediaBundle\Resizer\ResizerInterface;
use Imagine\Image\ImageInterface;
use Imagine\Exception\InvalidArgumentException;
use Sonata\MediaBundle\Metadata\MetadataBuilderInterface;

class ThumbnailResizer implements ResizerInterface
{
    protected $adapter;
    protected $mode;
    protected $metadata;

    /**
     * @param ImagineInterface $adapter
     * @param string $mode
     */
    public function __construct(ImagineInterface $adapter, $mode, MetadataBuilderInterface $metadata)
    {
        $this->adapter = $adapter;
        $this->mode = $mode;
        $this->metadata = $metadata;
    }

    /**
     * {@inheritdoc}
     */
    public function resize(MediaInterface $media, File $in, File $out, $format, array $settings)
    {
        if (!(isset($settings['width']) && $settings['width'])) {
            throw new \RuntimeException(
                sprintf(
                    'Width parameter is missing in context "%s" for provider "%s"',
                    $media->getContext(),
                    $media->getProviderName()
                )
            );
        }

        $image = $this->adapter->load($in->getContent());
        $size = $image->getSize();
        $thumbSize = $this->getBox($media, $settings);
        $origWidth = $size->getWidth();
        $origHeight = $size->getHeight();
        $thumbWidth = $thumbSize->getWidth();
        $thumbHeight = $thumbSize->getHeight();
        ;

        // FIXME: inject an instnce of extented \Imagine\Gd\Image with cropThumbnail implemented there
        if ($image instanceof \Imagine\Gd\Image) {
            $origAspect = $origWidth / $origHeight;
            $thumbAspect = $thumbWidth / $thumbHeight;
            if ($origAspect >= $thumbAspect) {
                // if image is wider than thumbnail (in aspect ratio sense)
                $newHeight = $thumbHeight;
                $newWidth = $origWidth / ($origHeight / $thumbHeight);
            } else {
                // if the thumbnail is wider than the image
                $newWidth = $thumbWidth;
                $newHeight = $origHeight / ($origWidth / $thumbWidth);
            }

            $thumb = $this->adapter->create($thumbSize);

            imagealphablending($image->getGdResource(), true);
            imagealphablending($thumb->getGdResource(), false);
            imagesavealpha($thumb->getGdResource(), true );

            // resize and crop
            imagecopyresampled(
                $thumb->getGdResource(),
                $image->getGdResource(),
                0 - ($newWidth - $thumbWidth) / 2, // center the image horizontally
                0 - ($newHeight - $thumbHeight) / 2, // center the image vertically
                0, 0,
                $newWidth, $newHeight,
                $origWidth, $origHeight
            );

            // copied from \Imagine\Gd\Image:resize
            imagealphablending($image->getGdResource(), false);
            imagealphablending($thumb->getGdResource(), false);

        } else {
            if (($origWidth < $thumbWidth || $origHeight < $thumbHeight)) {
                // resize first
                $ratio = max($thumbWidth / $origWidth, $thumbHeight / $origHeight);
                $resizeFilter = new Resize(new Box(round($origWidth * $ratio), round($origHeight * $ratio)));
                $image = $resizeFilter->apply($image);
            }

            $thumb = $image
                ->thumbnail($thumbSize, $this->mode);
        }

        $content = $thumb
            ->get($format, array('quality' => $settings['quality']));

        $out->setContent($content, $this->metadata->get($media, $out->getName()));
    }

    /**
     * {@inheritdoc}
     */
    public function getBox(MediaInterface $media, array $settings)
    {
        $size = $media->getBox();

        if ($settings['width'] == null && $settings['height'] == null) {
            throw new \RuntimeException(sprintf('Width/Height parameter is missing in context "%s" for provider "%s". Please add at least one parameter.', $media->getContext(), $media->getProviderName()));
        }

        if ($settings['height'] == null) {
            $settings['height'] = (int) ($settings['width'] * $size->getHeight() / $size->getWidth());
        }

        if ($settings['width'] == null) {
            $settings['width'] = (int) ($settings['height'] * $size->getWidth() / $size->getHeight());
        }

        return $this->computeBox($media, $settings);
    }

    /**
     * @throws InvalidArgumentException
     *
     * @param MediaInterface $media
     * @param array $settings
     *
     * @return Box
     */
    private function computeBox(MediaInterface $media, array $settings)
    {
        return new Box($settings['width'], $settings['height']);
    }
}
