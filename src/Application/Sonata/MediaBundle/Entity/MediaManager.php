<?php

namespace Application\Sonata\MediaBundle\Entity;

use Sonata\MediaBundle\Entity\MediaManager as BaseMediaManager;

class MediaManager extends BaseMediaManager
{
    const CONOR_MEDIA_CATEGORY = 'carousel';

    public function getMediaItems4ConorPage()
    {
        $qb = $this->getRepository()->createQueryBuilder('media');

        $qb->select('media')
            ->leftJoin('media.category', 'category')
            ->where('category.name=:category')
            ->andWhere('media.enabled=1')
            ->setParameter(':category', self::CONOR_MEDIA_CATEGORY)
        ;

        return $qb->getQuery()->getResult();
    }
}
