<?php

namespace Application\Sonata\MediaBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;


class OverrideImageProviderCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if ($container->hasDefinition('sonata.media.provider.image')) {
            $container->getDefinition('sonata.media.provider.image')
                ->setClass('Application\Sonata\MediaBundle\Provider\ImageProvider')
            ;
        }
    }
}