<?php

namespace Application\Sonata\MediaBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

/**
 * Class ExtendDefaultPageServiceCompilerPass.
 */
class ExtendDefaultMediaServiceCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if ($container->hasDefinition('sonata.media.manager.media')) {
            $container->getDefinition('sonata.media.manager.media')
                ->setClass('Application\Sonata\MediaBundle\Entity\MediaManager')
            ;
        }
    }
}
