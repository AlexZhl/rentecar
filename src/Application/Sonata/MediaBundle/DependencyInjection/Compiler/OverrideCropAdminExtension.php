<?php

namespace Application\Sonata\MediaBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

/**
 * Class ExtendDefaultPageServiceCompilerPass.
 */
class OverrideCropAdminExtension implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if ($container->hasDefinition('j_maitan_sonata_media_crop.crop.extension')) {
            $container->getDefinition('j_maitan_sonata_media_crop.crop.extension')
                ->setClass('Application\Sonata\MediaBundle\Admin\CropAdminExtension')
            ;
        }
    }
}
