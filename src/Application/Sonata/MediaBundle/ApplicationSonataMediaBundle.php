<?php

namespace Application\Sonata\MediaBundle;

use Application\Sonata\MediaBundle\DependencyInjection\Compiler\OverrideCropAdminExtension;
use Application\Sonata\MediaBundle\DependencyInjection\Compiler\OverrideImageProviderCompilerPass;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Application\Sonata\MediaBundle\DependencyInjection\Compiler\ExtendDefaultMediaServiceCompilerPass;

class ApplicationSonataMediaBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new OverrideImageProviderCompilerPass());
        $container->addCompilerPass(new ExtendDefaultMediaServiceCompilerPass());
        $container->addCompilerPass(new OverrideCropAdminExtension());
    }
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'SonataMediaBundle';
    }
}
