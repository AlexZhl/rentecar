<?php

namespace Application\Sonata\PageBundle\Entity;

use Doctrine\DBAL\Cache\QueryCacheProfile;
use Sonata\PageBundle\Entity\SnapshotManager as BaseSnapshotManager;

/**
 * Class SnapshotManager.
 */
class SnapshotManager extends BaseSnapshotManager
{
    protected $ttl = 120;

    /**
     * {@inheritdoc}
     */
    public function findEnableSnapshot(array $criteria)
    {
        $date = new \Datetime();
        $parameters = array(
            //'publicationDateStart' => $date,
            //'publicationDateEnd'   => $date,
        );

        $query = $this->getRepository()
            ->createQueryBuilder('s')
            ->andWhere('s.publicationDateStart <= UTC_TIMESTAMP() AND ( s.publicationDateEnd IS NULL OR s.publicationDateEnd >= UTC_TIMESTAMP() )');

        if (isset($criteria['site'])) {
            $query->andWhere('s.site = :site');
            $parameters['site'] = $criteria['site'];
        }

        if (isset($criteria['pageId'])) {
            $query->andWhere('s.page = :page');
            $parameters['page'] = $criteria['pageId'];
        } elseif (isset($criteria['url'])) {
            $query->andWhere('s.url = :url');
            $parameters['url'] = $criteria['url'];
        } elseif (isset($criteria['routeName'])) {
            $query->andWhere('s.routeName = :routeName');
            $parameters['routeName'] = $criteria['routeName'];
        } elseif (isset($criteria['pageAlias'])) {
            $query->andWhere('s.pageAlias = :pageAlias');
            $parameters['pageAlias'] = $criteria['pageAlias'];
        } elseif (isset($criteria['name'])) {
            $query->andWhere('s.name = :name');
            $parameters['name'] = $criteria['name'];
        } else {
            throw new \RuntimeException('please provide a `pageId`, `url`, `routeName` or `name` as criteria key');
        }

        $query->setMaxResults(1);
        $query->setParameters($parameters);
        $query = $query->getQuery();

        $cache = $this->getEntityManager()->getConfiguration()->getResultCacheImpl();
        $hydrationCacheProfile = new QueryCacheProfile($this->ttl, null, $cache);

        $query->useResultCache(true, $this->ttl);
        $query->setHydrationCacheProfile($hydrationCacheProfile);

        return $query->getOneOrNullResult();
    }

    /**
     * @param $ttl
     */
    public function setCacheTtl($ttl)
    {
        $this->ttl = $ttl;
    }
}
