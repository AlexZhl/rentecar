<?php

namespace Application\Sonata\PageBundle\Entity;

use Sonata\PageBundle\Entity\BaseBlock as BaseBlock;

class Block extends BaseBlock
{

    /**
     * @var int
     */
    protected $id;

    /**
     * Get id.
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    public function preUpdate()
    {
        parent::preUpdate();
        $media = $this->getSetting('mediaId');
        if (is_object($media)) {
            $this->setSetting('mediaId', $media->getId());
        }
    }

}
