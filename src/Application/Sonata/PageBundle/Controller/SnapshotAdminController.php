<?php

namespace Application\Sonata\PageBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sonata\PageBundle\Controller\SnapshotAdminController as BaseSnapshotAdminController;

class SnapshotAdminController extends BaseSnapshotAdminController
{
    /**
     * {@inheritdoc}
     */
    public function createAction(Request $request = null)
    {
        if (!$this->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }

        return parent::createAction($request);
    }
}
