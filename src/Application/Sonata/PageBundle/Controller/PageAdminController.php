<?php

namespace Application\Sonata\PageBundle\Controller;

use Application\Sonata\PageBundle\Entity\Snapshot;
use Sonata\PageBundle\Controller\PageAdminController as BasePageAdminController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class PageAdminController.
 */
class PageAdminController extends BasePageAdminController
{
    /**
     * Save and publish current page.
     *
     * @param Request|null $request
     */
    public function saveAction(Request $request = null)
    {
        if (
            false === $this->admin->isGranted('EDIT')
            || false === $this->get('sonata.page.admin.block')->isGranted('LIST')
            || false === $this->isGranted('ROLE_SUPER_ADMIN')
        ) {
            throw new AccessDeniedException();
        }

        $id = $request->get($this->admin->getIdParameter());
        $page = $this->admin->getObject($id);
        if (!$page) {
            throw new NotFoundHttpException(sprintf('unable to find the page with id : %s', $id));
        }

        $class = $this->get('sonata.page.manager.snapshot')->getClass();

        $pageManager = $this->get('sonata.page.manager.page');

        /** @var Snapshot $snapshot */
        $snapshot = new $class();
        $snapshot->setPage($page);
        $snapshotManager = $this->get('sonata.page.manager.snapshot');
        $transformer = $this->get('sonata.page.transformer');

        $page->setEdited(false);

        $snapshot = $transformer->create($page);

        $snapshotAdmin = $this->get('sonata.page.admin.snapshot');
        $snapshotAdmin->create($snapshot);

        $pageManager->save($page);

        $snapshotManager->enableSnapshots(array($snapshot));

        $this->get('session')->getFlashBag()->add('sonata_flash_success', 'Saved and published successfully');

        return $this->redirect($this->generateUrl('admin_sonata_page_page_compose', ['id' => $id]));
    }

    /**
     * {@inheritdoc}
     *
     * @see \Sonata\PageBundle\Controller\PageAdminController::composeAction()
     */
    public function composeAction(Request $request = null)
    {
        $this->admin->checkAccess('compose');
        if (false === $this->get('sonata.page.admin.block')->isGranted('LIST')) {
            throw new AccessDeniedException();
        }

        $id = $request->get($this->admin->getIdParameter());
        $page = $this->admin->getObject($id);
        if (!$page) {
            throw new NotFoundHttpException(sprintf('unable to find the page with id : %s', $id));
        }

        $containers = array();
        $orphanContainers = array();
        $children = array();

        $templateManager = $this->get('sonata.page.template_manager');
        $template = $templateManager->get($page->getTemplateCode());
        $templateContainers = $template->getContainers();

        foreach ($templateContainers as $id => $container) {
            $containers[$id] = array(
                'area' => $container,
                'block' => false,
            );
        }

        // 'attach' containers to corresponding template area, otherwise add it to orphans
        foreach ($page->getBlocks() as $block) {
            $blockCode = $block->getSetting('code');
            if ($block->getParent() === null) {
                if (isset($containers[$blockCode])) {
                    $containers[$blockCode]['block'] = $block;
                } else {
                    $orphanContainers[] = $block;
                }
            } else {
                $children[] = $block;
            }
        }

        // searching for block defined in template which are not created
        $blockInteractor = $this->get('sonata.page.block_interactor');

        foreach ($containers as $id => $container) {
            if ($container['block'] === false && $templateContainers[$id]['shared'] === false) {
                $blockContainer = $blockInteractor->createNewContainer(array(
                    'page' => $page,
                    'name' => $templateContainers[$id]['name'],
                    'code' => $id,
                ));

                $containers[$id]['block'] = $blockContainer;
            }
        }

        $csrfProvider = $this->get('form.csrf_provider');

        return $this->render('SonataPageBundle:PageAdmin:compose.html.twig', array(
            'object' => $page,
            'action' => 'edit',
            'template' => $template,
            'page' => $page,
            'containers' => $containers,
            'orphanContainers' => $orphanContainers,
            'csrfTokens' => array(
                'remove' => $csrfProvider->generateCsrfToken('sonata.delete'),
            ),
            'hasAccessToPublish' => $this->isGranted('ROLE_SUPER_ADMIN'),
        ));
    }

    /**
     * To keep backwards compatibility with older Sonata Admin code.
     *
     * @internal
     */
    private function resolveRequest(Request $request = null)
    {
        if (null === $request) {
            return $this->getRequest();
        }

        return $request;
    }
}
