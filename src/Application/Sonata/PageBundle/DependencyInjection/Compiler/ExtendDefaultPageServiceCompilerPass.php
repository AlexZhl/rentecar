<?php

namespace Application\Sonata\PageBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Parameter;

/**
 * Class ExtendDefaultPageServiceCompilerPass.
 */
class ExtendDefaultPageServiceCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if ($container->hasDefinition('sonata.page.kernel.exception_listener')) {
            $container->getDefinition('sonata.page.kernel.exception_listener')
                 ->setClass('Application\Sonata\PageBundle\Listener\ExceptionListener')
             ;
        }

        if ($container->hasDefinition('sonata.page.response_listener')) {
            $container->getDefinition('sonata.page.response_listener')
                ->setClass('Application\Sonata\PageBundle\Listener\ResponseListener')
             ;
        }

        if ($container->hasDefinition('sonata.page.manager.snapshot')) {
            $container->getDefinition('sonata.page.manager.snapshot')
                ->addMethodCall('setCacheTtl', [new Parameter('doctrine_cache_ttl')])
            ;
        }

        if ($container->hasDefinition('sonata.page.site.selector.host')) {
            $container->getDefinition('sonata.page.site.selector.host')
                ->addMethodCall('setCacheTtl', [new Parameter('doctrine_cache_ttl')])
            ;
        }
    }
}
