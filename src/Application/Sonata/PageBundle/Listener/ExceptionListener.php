<?php

namespace Application\Sonata\PageBundle\Listener;

use Sonata\PageBundle\Listener\ExceptionListener as BaseExceptionListener;
use Sonata\PageBundle\Exception\InternalErrorException;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * ExceptionListener.
 */
class ExceptionListener extends BaseExceptionListener
{
    /**
     * Handles a kernel exception.
     *
     * @param GetResponseForExceptionEvent $event
     *
     * @throws \Exception
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if ($event->getException() instanceof InternalErrorException) {
            $event->setException(new NotFoundHttpException());
        }
        parent::onKernelException($event);
    }
}
