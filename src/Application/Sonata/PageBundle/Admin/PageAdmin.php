<?php

namespace Application\Sonata\PageBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\PageBundle\Admin\PageAdmin as BasePageAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Application\Sonata\PageBundle\Entity\Page;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;

/**
 * Class PageAdmin.
 */
class PageAdmin extends BasePageAdmin
{
    /**
     * Adds new 'save' route action to existing routes collection.
     *
     * @param RouteCollection $collection
     */
    public function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);

        $collection->add('save', '{id}/save', array(
            'id' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('decorate', null, array('editable' => true))
            ->add('enabled', null, array('editable' => true))
            ->add('edited', null, array('editable' => true))
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        // define group zoning
        $formMapper
            ->with('form_page.group_main_label', array('class' => 'col-md-6'))->end()
            ->with('form_page.group_seo_label', array('class' => 'col-md-6'))->end()
            ->with('form_page.group_advanced_label', array('class' => 'col-md-6'))->end()
        ;

        if (!$this->getSubject() || (!$this->getSubject()->isInternal() && !$this->getSubject()->isError())) {
            $formMapper
                ->with('form_page.group_main_label')
                ->add('url', 'text', array('attr' => array('readonly' => 'readonly')))
                ->end()
            ;
        }

        if ($this->hasSubject() && !$this->getSubject()->getId()) {
            $formMapper
                ->with('form_page.group_main_label')
                ->add('site', null, array('required' => true, 'read_only' => true, 'attr' => ['class' => 'hidden'], 'label' => false))
                ->end()
            ;
        }

        $formMapper
            ->with('form_page.group_main_label')
            ->add('name')
            ->add('enabled', null, array('required' => false))
            ->add('position')
            ->end()
        ;

        if ($this->hasSubject() && !$this->getSubject()->isInternal()) {
            $formMapper
                ->with('form_page.group_main_label')
                //->add('type', 'sonata_page_type_choice', array('required' => false))
                ->end()
            ;
        }

        $formMapper
            ->with('form_page.group_main_label')
            ->add('templateCode', 'sonata_page_template', array('required' => true, 'attr' => ['class' => 'hidden'], 'label' => false))
            ->end()
        ;

        if (!$this->getSubject() || ($this->getSubject() && $this->getSubject()->getParent()) || ($this->getSubject() && !$this->getSubject()->getId())) {
            $formMapper
            ->with('form_page.group_main_label')
            ->add('parent', 'sonata_page_selector', array(
                'page' => $this->getSubject() ?: null,
                'site' => $this->getSubject() ? $this->getSubject()->getSite() : null,
                'model_manager' => $this->getModelManager(),
                'class' => $this->getClass(),
                'required' => false,
                'filter_choice' => array('hierarchy' => 'root'),
            ), array(
                'admin_code' => $this->getCode(),
                'link_parameters' => array(
                    'siteId' => $this->getSubject() ? $this->getSubject()->getSite()->getId() : null,
                ),
            ))
            ->end()
            ;
        }

        if (!$this->getSubject() || !$this->getSubject()->isDynamic()) {
            $formMapper
            ->with('form_page.group_main_label')
            //->add('pageAlias', null, array('required' => false))
//             ->add('target', 'sonata_page_selector', array(
//                 'page'          => $this->getSubject() ?: null,
//                 'site'          => $this->getSubject() ? $this->getSubject()->getSite() : null,
//                 'model_manager' => $this->getModelManager(),
//                 'class'         => $this->getClass(),
//                 'filter_choice' => array('request_method' => 'all'),
//                 'required'      => false,
//             ), array(
//                 'admin_code'      => $this->getCode(),
//                 'link_parameters' => array(
//                     'siteId' => $this->getSubject() ? $this->getSubject()->getSite()->getId() : null,
//                 ),
//             ))
            ->end()
            ;
        }

        $formMapper
            ->with('form_page.group_main_label')
            ->add('templateCode', 'sonata_page_template', array('required' => true))
            ->end()
        ;

        if (!$this->getSubject() || !$this->getSubject()->isHybrid()) {
            $formMapper
            ->with('form_page.group_seo_label')
            ->add('slug', 'text',  array('required' => false))
            //->add('customUrl', 'text', array('required' => false))
            ->end()
            ;
        }

        $formMapper
            ->with('form_page.group_seo_label', array('collapsed' => true))
            ->add('title', null, array('required' => false))
            ->add('metaKeyword', 'textarea', array('required' => false))
            ->add('metaDescription', 'textarea', array('required' => false))
            ->end()
        ;

        if ($this->hasSubject() && !$this->getSubject()->isCms()) {
            $formMapper
            ->with('form_page.group_advanced_label', array('collapsed' => true))
//            ->add('decorate', null,  array('required' => false))
            ->end()
            ;
        }

        $formMapper
            ->with('form_page.group_advanced_label', array('collapsed' => true))
            ->add('javascript', null,  array('required' => false))
            ->add('stylesheet', null, array('required' => false))
            ->add('rawHeaders', null, array('required' => false))
            ->end()
        ;

        $formMapper->setHelps(array(
            'name' => $this->trans('help_page_name'),
            'url' => $this->trans('help_url'),
        ));
    }

    /**
     * {@inheritdoc}
     */
    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, array('edit'))) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;

        $id = $admin->getRequest()->get('id');

        $menu->addChild(
            $this->trans('sidemenu.link_edit_page'),
            array('uri' => $admin->generateUrl('edit', array('id' => $id)))
            );

        $menu->addChild(
            $this->trans('sidemenu.link_compose_page'),
            array('uri' => $admin->generateUrl('compose', array('id' => $id)))
            );

        $menu->addChild(
            $this->trans('sidemenu.link_list_blocks'),
            array('uri' => $admin->generateUrl('sonata.page.admin.page|sonata.page.admin.block.list', array('id' => $id)))
            );

        $menu->addChild(
            $this->trans('sidemenu.link_list_snapshots'),
            array('uri' => $admin->generateUrl('sonata.page.admin.page|sonata.page.admin.snapshot.list', array('id' => $id)))
            );

        if (!$this->getSubject()->isInternal()) {
            try {
                $menu->addChild(
                    $this->trans('view_page'),
                    array('uri' => $this->getRouteGenerator()->generate('page_slug', array('path' => $this->getSubject()->getUrl())))
                    );
            } catch (\Exception $e) {
                // avoid crashing the admin if the route is not setup correctly
                //                throw $e;
            }
        }
    }
}
