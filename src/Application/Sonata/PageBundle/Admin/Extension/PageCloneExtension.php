<?php

namespace Application\Sonata\PageBundle\Admin\Extension;

use Sonata\AdminBundle\Admin\AdminExtension;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\PageBundle\Model\PageManagerInterface;
use Sonata\BlockBundle\Model\BlockManagerInterface;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * @see https://github.com/sonata-project/SonataPageBundle/pull/514
 */
class PageCloneExtension extends AdminExtension
{
    /**
     * @var PageManagerInterface
     */
    protected $pageManager;

    /**
     * @var BlockManagerInterface
     */
    protected $blockManager;

    /**
     * {@inheritdoc}
     */
    public function postPersist(AdminInterface $admin, $object)
    {
        $isUseTemplate = $admin->getForm()->get('useTemplate')->getData();

        if ($isUseTemplate) {
            $page = $this->pageManager->findOneBy(array('routeName' => $this->templateRouteName));

            if ($page) {
                // clone page blocks
                $blocks = $this->blockManager->findBy(array('page' => $page));
                $blockClones = array();

                foreach ($blocks as $block) {
                    if ($block->getType() == 'sonata.media.block.gallery') {
                        continue;
                    }
                    $newBlock = clone $block;
                    $newBlock->setPage($object);
                    $blockClones[$block->getId()] = $newBlock;
                    $this->blockManager->save($newBlock, false);
                }

                foreach ($blockClones as $block) {
                    if ($block->getType() == 'sonata.media.block.gallery') {
                        continue;
                    }
                    if ($block->getParent()) {
                        $block->setParent($blockClones[$block->getParent()->getId()]);
                    }
                    $this->blockManager->save($newBlock, false);
                }

                $this->blockManager->getEntityManager()->flush();
            }
        }
    }

    public function configureFormFields(FormMapper $formMapper)
    {
        if ((!$formMapper->getAdmin()->getSubject()) || (!$formMapper->getAdmin()->getSubject()->getId())) {
            $formMapper
                ->with('form_page.group_main_label')
                    ->add('useTemplate', 'checkbox', array(
                        'data' => true,
                        'mapped' => false,
                        'required' => false,
                    ))
                ->end()
            ;
        }
    }

    /**
     * @param PageManagerInterface $pageManager
     */
    public function setPageManager(PageManagerInterface $pageManager)
    {
        $this->pageManager = $pageManager;
    }

    /**
     * @param BlockManagerInterface $blockManager
     */
    public function setBlockManager(BlockManagerInterface $blockManager)
    {
        $this->blockManager = $blockManager;
    }

    /**
     * @param string $templateRouteName
     */
    public function setPageTemplateRouteName($templateRouteName)
    {
        $this->templateRouteName = $templateRouteName;
    }
}
