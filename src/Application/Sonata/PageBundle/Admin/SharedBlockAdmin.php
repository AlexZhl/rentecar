<?php

namespace Application\Sonata\PageBundle\Admin;

use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\PageBundle\Entity\BaseBlock;
use Sonata\PageBundle\Admin\SharedBlockAdmin as BasesSharedBlockAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;

class SharedBlockAdmin extends BasesSharedBlockAdmin
{
    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('type')
            //->add('enabled', null, array('editable' => true))
            ->add('updatedAt')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var BaseBlock $block */
        $block = $this->getSubject();

        // New block
        if ($block->getId() === null) {
            $block->setType($this->request->get('type'));
        }

        $formMapper
            ->with('form.field_group_general')
            ->add('name', null, array('required' => true))
//            ->add('enabled')
        ->end();

        $formMapper->with('form.field_group_options');

        /** @var BaseBlockService $service */
        $service = $this->blockManager->get($block);

        if ($block->getId() > 0) {
            $service->buildEditForm($formMapper, $block);
        } else {
            $service->buildCreateForm($formMapper, $block);
        }

        $formMapper->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        ->add('name')
        ;
    }
}
