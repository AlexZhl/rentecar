<?php

namespace Application\Sonata\PageBundle\BlockService;

use Sonata\BlockBundle\Block\Service\TextBlockService as BaseTextBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Symfony\Component\HttpFoundation\Response;

class TextBlockService extends BaseTextBlockService
{
    /** @var Quizzle\CoreBundle\Manager\QuizManager **/
    private $quizManager = null;

    /** @var Symfony\Component\Translation\Translator **/
    private $translator = null;

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $locale = $this->translator->getLocale();
        $settings = $blockContext->getSettings();
        $quiz = $this->quizManager->getCurrent();

        $formatter = new \IntlDateFormatter($locale, \IntlDateFormatter::LONG, \IntlDateFormatter::NONE);

        if (($quiz) && (!empty($settings['content']))) {
            $content = $settings['content'];

            $content = str_replace('{{quiz_start_date}}', $formatter->format($quiz->getStartedAt()), $content);
            $content = str_replace('{{quiz_end_date}}', $formatter->format($quiz->getFinishAt()), $content);

            $settings['content'] = $content;
        }

        return $this->renderResponse($blockContext->getTemplate(), array(
            'block' => $blockContext->getBlock(),
            'settings' => $settings,
        ), $response);
    }

    /**
     * @param Quizzle\CoreBundle\Manager\QuizManager $quizManager
     */
    public function setQuizManager(\Quizzle\CoreBundle\Manager\QuizManager $quizManager)
    {
        $this->quizManager = $quizManager;
    }

    /**
     * @param Symfony\Component\Translation\Translator $translator
     */
    public function setTranslator($translator)
    {
        $this->translator = $translator;
    }
}
