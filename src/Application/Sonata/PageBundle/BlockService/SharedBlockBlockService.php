<?php

namespace Application\Sonata\PageBundle\BlockService;

use Sonata\PageBundle\Block\SharedBlockBlockService as BaseSharedBlockBlockService;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * {@inheritdoc}
 */
class SharedBlockBlockService extends BaseSharedBlockBlockService
{
    /**
     * {@inheritdoc}
     */
    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'template' => 'SonataPageBundle:Block:block_shared_block.html.twig',
            'blockId' => null,
        ));
    }
}
