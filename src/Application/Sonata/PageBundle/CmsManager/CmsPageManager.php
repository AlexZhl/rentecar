<?php

namespace Application\Sonata\PageBundle\CmsManager;

use Sonata\PageBundle\CmsManager\CmsPageManager as BaseCmsPageManager;
use Sonata\PageBundle\Model\SiteInterface;

class CmsPageManager extends BaseCmsPageManager
{

    /**
     * {@inheritdoc}
     */
    public function getInternalRoute(SiteInterface $site, $pageName)
    {
        if (substr($pageName, 0, 5) == 'error') {
            throw new \RuntimeException(sprintf('Illegal internal route name : %s, an internal page cannot start with `error`', $pageName));
        }

        $routeName = sprintf('_page_internal_%s', $pageName);

        $page = $this->getPageByRouteName($site, $routeName);

        return $page;
    }

}
