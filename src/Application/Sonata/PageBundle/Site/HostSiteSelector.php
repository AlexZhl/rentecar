<?php

namespace Application\Sonata\PageBundle\Site;

use Doctrine\DBAL\Cache\QueryCacheProfile;
use Sonata\PageBundle\Site\HostSiteSelector as BaseHostSiteSelector;
use Symfony\Component\HttpFoundation\Request;

class HostSiteSelector extends BaseHostSiteSelector
{
    protected $ttl = 120;

    /**
     * @param Request $request
     *
     * @return SiteInterface[]
     */
    protected function getSites(Request $request)
    {
        $query = $this->siteManager->getEntityManager()->createQueryBuilder()
            ->select('site')
            ->from($this->siteManager->getClass(), 'site')
            ->where('site.host in (:hosts)')
            ->setParameter('hosts', array($request->getHost(), 'localhost'))
            ->andWhere('site.enabled = :enabled')
            ->setParameter('enabled', true)
            ->orderBy('site.isDefault', 'desc')
            ->getQuery()
        ;

        $cache = $this->siteManager->getEntityManager()->getConfiguration()->getResultCacheImpl();
        $hydrationCacheProfile = new QueryCacheProfile($this->ttl, null, $cache);

        $query->useResultCache(true, $this->ttl);
        $query->setHydrationCacheProfile($hydrationCacheProfile);

        return $query->getResult();
    }

    /**
     * @param $ttl
     */
    public function setCacheTtl($ttl)
    {
        $this->ttl = $ttl;
    }
}
