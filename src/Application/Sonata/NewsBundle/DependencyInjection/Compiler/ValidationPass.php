<?php

namespace Application\Sonata\NewsBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class ValidationPass implements CompilerPassInterface
{

    public function process(ContainerBuilder $container)
    {
        $validatorBuilder = $container->getDefinition('validator.builder');

        foreach ($validatorBuilder->getMethodCalls('addXmlMappings') as $call) {
            if ('addXmlMappings' == $call[0]) {
                $validatorBuilder->removeMethodCall($call[0]);
                $arguments = array();
                foreach ($call[1][0] as $path) {
                    if (false !== strpos($path, 'news-bundle' . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'validation.xml')) {
                        continue;
                    }
                    $arguments[] = $path;
                }
                $validatorBuilder->addMethodCall($call[0], array($arguments));

                break;
            }
        }
    }

}
