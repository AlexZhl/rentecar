<?php

namespace Application\Sonata\NewsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class NewsPost extends Post
{
    /**
     * @var int $discriminatorStr a field for elasticsearch
     */
    protected $discriminatorStr = 'news';

    /**
     * @var string $subTitle
     */
    protected $subTitle;



    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get subTitle
     *
     * @return string $subTitle
     */
    public function getSubTitle()
    {
        return $this->subTitle;
    }

    /**
     * Set subTitle
     *
     * @param $subTitle
     */
    public function setSubTitle($subTitle)
    {
        $this->subTitle = $subTitle;
    }

}
