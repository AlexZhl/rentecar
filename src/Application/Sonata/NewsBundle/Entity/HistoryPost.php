<?php

namespace Application\Sonata\NewsBundle\Entity;


class HistoryPost extends Post
{
    /**
     * @var int $discriminatorStr a field for elasticsearch
     */
    protected $discriminatorStr = 'history';

    /**
     * @var string $subTitle
     */
    protected $subTitle;

    /**
     * @var string $date
     */
    protected $date;

    private $position;

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get subTitle
     *
     * @return string $subTitle
     */
    public function getSubTitle()
    {
        return $this->subTitle;
    }

    /**
     * Set subTitle
     *
     * @param $subTitle
     */
    public function setSubTitle($subTitle)
    {
        $this->subTitle = $subTitle;
    }


    /**
     * Get date
     *
     * @return string $date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set date
     *
     * @param $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }
}
