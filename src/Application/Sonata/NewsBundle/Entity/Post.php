<?php

namespace Application\Sonata\NewsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Sonata\NewsBundle\Entity\BasePost as BasePost;
use Doctrine\Common\Collections\Criteria;
use Application\Sonata\ClassificationBundle\Entity\Context;

class Post extends BasePost
{

    const ENABLED = 1;
    const FEATURED = 1;
    const HERO = 1;

    //FR-SP-04
    const SEARCH_POSTS_PER_PAGE_PC = 5;
    const POSTS_PER_PAGE_PC = 6;
    const POSTS_PER_PAGE_MOB = 6;

    /**
     * @var int $id
     */
    protected $id;

    /**
     * @var ArrayCollection $categories
     */
    protected $categories;

    /**
     * @var int $discriminatorStr a field for elasticsearch
     */
    protected $discriminatorStr;

    /**
     * @var int $post_category
     */
    protected $post_category;

    /**
     * @var string $post_seo_title
     */
    protected $post_seo_title;

    /**
     * @var string $post_seo_meta_keywords
     */
    protected $post_seo_meta_keywords;

    /**
     * @var string $post_seo_meta_description
     */
    protected $post_seo_meta_description;

    /**
     * @param mixed $post_category
     */
    public function setPostCategory($post_category)
    {
        $this->post_category = $post_category;
    }

    /**
     * @return mixed
     */
    public function getPostCategory()
    {
        return $this->post_category;
    }

    /**
     * {@inheritdoc}
     */
    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->commentsDefaultStatus = 0;
        $this->commentsEnabled = 0;
        $this->contentFormatter = 'richhtml';

        parent::__construct();
    }

    /**
     * Get discriminatorStr
     *
     * @return string $discriminatorStr
     */
    public function getDiscriminatorStr()
    {
        return $this->discriminatorStr;
    }

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get category
     *
     * @return ArrayCollection $categories
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set category
     *
     * @param $categories
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
    }


    public function getCacheId()
    {
        return 'post';
    }

    /**
     * @param string $post_seo_title
     */
    public function setPostSeoTitle($post_seo_title)
    {
        $this->post_seo_title = $post_seo_title;
    }

    /**
     * @return string
     */
    public function getPostSeoTitle()
    {
        return $this->post_seo_title;
    }

    /**
     * @param string $post_seo_meta_keywords
     */
    public function setPostSeoMetaKeywords($post_seo_meta_keywords)
    {
        $this->post_seo_meta_keywords = $post_seo_meta_keywords;
    }

    /**
     * @return string
     */
    public function getPostSeoMetaKeywords()
    {
        return $this->post_seo_meta_keywords;
    }

    /**
     * @param string $post_seo_meta_description
     */
    public function setPostSeoMetaDescription($post_seo_meta_description)
    {
        $this->post_seo_meta_description = $post_seo_meta_description;
    }

    /**
     * @return string
     */
    public function getPostSeoMetaDescription()
    {
        return $this->post_seo_meta_description;
    }
}
