<?php

namespace Application\Sylius\SettingsBundle;

use Application\Sylius\SettingsBundle\DependencyInjection\Compiler\OverrideSyliusSettingsManagerClassCompilerPass;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class ApplicationSyliusSettingsBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'SyliusSettingsBundle';
    }

    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new OverrideSyliusSettingsManagerClassCompilerPass());
    }
}
