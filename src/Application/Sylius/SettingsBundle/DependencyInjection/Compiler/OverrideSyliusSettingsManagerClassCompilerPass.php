<?php

namespace Application\Sylius\SettingsBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

class OverrideSyliusSettingsManagerClassCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if ($container->hasParameter('sylius.settings.manager.class')) {
            $container->setParameter('sylius.settings.manager.class', 'Application\\Sylius\\SettingsBundle\\Manager\\SettingsManager');
        }
    }
}