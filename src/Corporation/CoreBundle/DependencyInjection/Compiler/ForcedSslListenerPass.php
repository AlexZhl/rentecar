<?php

namespace Corporation\CoreBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class ForcedSslListenerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if ($container->hasDefinition('nelmio_security.forced_ssl_listener')) {
            $container->getDefinition('nelmio_security.forced_ssl_listener')
                ->setClass('Corporation\CoreBundle\EventListener\ForcedSslListener')
            ;
        }
    }
}
