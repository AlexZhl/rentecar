<?php

namespace Corporation\CoreBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class FormTypeCsrfExtensionPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if ($container->hasDefinition('form.type_extension.csrf')) {
            $container->getDefinition('form.type_extension.csrf')
                ->setClass('Corporation\CoreBundle\Form\Extension\FormTypeCsrfExtension')
                ->addMethodCall('setSession', [new Reference('session')])
            ;
        }
    }
}
