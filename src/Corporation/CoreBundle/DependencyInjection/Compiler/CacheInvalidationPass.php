<?php

namespace Corporation\CoreBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class CacheInvalidationPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if ($container->hasDefinition('sonata.cache.recorder')) {
            $container->getDefinition('sonata.cache.recorder')
                ->setClass('Corporation\CoreBundle\Cache\Invalidation\Recorder')
            ;
        }

        if ($container->hasDefinition('sonata.cache.orm.event_subscriber')) {
            $container->getDefinition('sonata.cache.orm.event_subscriber')
                ->setClass('Corporation\CoreBundle\Cache\Listener\DoctrineORMListener')
            ;
        }
    }
}
