<?php

namespace Corporation\CoreBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class CookieSessionHandlerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if ($container->hasDefinition('nelmio_security.session.handler')) {
            $container->getDefinition('nelmio_security.session.handler')
                ->setClass('Corporation\CoreBundle\Session\CookieSessionHandler')
                ->addMethodCall('setRoleHierarchy', [$container->getParameter('security.role_hierarchy.roles')])
                ->addMethodCall('setCookieLifeTimeRoles', $container->getParameter('corporation_core.cookie_lifetime_roles'))
            ;
        }
    }
}
