<?php

namespace Corporation\CoreBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class CorporationCoreExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('corporation_core.cookie_lifetime_roles', $config, $config['cookie_lifetime_roles']);

        if ($container->hasParameter('calendar_date_format')) {
            $container->setParameter(
                'jquery_ui_date_format',
                $this->dateFormatPHPToJQueryUI($container->getParameter('calendar_date_format'))
            );
        } else {
            $container->setParameter('jquery_ui_date_format', 'dd/mm/yy');
        }

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        $loader->load('managers.yml');
        $loader->load('admin.yml');
    }

    protected function dateFormatPhpToJQueryUi($phpFormat)
    {
        $symbolsMatching = array(
            // Day
            'd' => 'dd',
            'D' => 'D',
            'j' => 'd',
            'l' => 'DD',
            'N' => '',
            'S' => '',
            'w' => '',
            'z' => 'o',
            // Week
            'W' => '',
            // Month
            'F' => 'MM',
            'm' => 'mm',
            'M' => 'M',
            'n' => 'm',
            't' => '',
            // Year
            'L' => '',
            'o' => '',
            'Y' => 'yy',
            'y' => 'y',
            // Time
            'a' => '',
            'A' => '',
            'B' => '',
            'g' => '',
            'G' => '',
            'h' => '',
            'H' => '',
            'i' => '',
            's' => '',
            'u' => ''
        );
        $jqueryFormat = "";
        $escaping = false;
        for($i = 0; $i < strlen($phpFormat); $i++)
        {
            $char = $phpFormat[$i];
            if($char === '\\') // PHP date format escaping character
            {
                $i++;
                if($escaping) $jqueryFormat .= $phpFormat[$i];
                else $jqueryFormat .= '\'' . $phpFormat[$i];
                $escaping = true;
            }
            else
            {
                if($escaping) { $jqueryFormat .= "'"; $escaping = false; }
                if(isset($symbolsMatching[$char]))
                    $jqueryFormat .= $symbolsMatching[$char];
                else
                    $jqueryFormat .= $char;
            }
        }
        return $jqueryFormat;
    }
}
