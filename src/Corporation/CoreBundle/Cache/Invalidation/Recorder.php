<?php

namespace Corporation\CoreBundle\Cache\Invalidation;

use Doctrine\Common\Util\ClassUtils;
use Sonata\Cache\Invalidation\Recorder as BaseRecorder;

class Recorder extends BaseRecorder
{
    /**
     * @param $object
     */
    public function add($object)
    {
        if (is_array($object)) {
            list($class, $identifier) = $object;
        } else {
            $class = ClassUtils::getClass($object);
            $identifier = $this->collectionIdentifiers->getIdentifier($object);
        }

        if ($identifier === false) {
            return;
        }

        if (!isset($this->stack[$this->current][$class])) {
            $this->stack[$this->current][$class] = array();
        }

        if (!in_array($identifier, $this->stack[$this->current][$class])) {
            $this->stack[$this->current][$class][] = $identifier;
        }
    }
}
