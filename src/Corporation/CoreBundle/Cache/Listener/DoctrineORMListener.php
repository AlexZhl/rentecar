<?php

namespace Corporation\CoreBundle\Cache\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Sonata\CacheBundle\Invalidation\DoctrineORMListenerContainerAware;

class DoctrineORMListener extends DoctrineORMListenerContainerAware
{
    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return array(
            Events::preRemove,
            Events::preUpdate,
            Events::prePersist,
        );
    }

    /**
     * @param \Doctrine\ORM\Event\LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $this->preUpdate($args);
    }
}
