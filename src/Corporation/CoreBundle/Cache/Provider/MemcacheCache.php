<?php

namespace Corporation\CoreBundle\Cache\Provider;

use Doctrine\Common\Cache\MemcacheCache as BaseMemcacheCache;

/**
 * Memcache cache provider for Doctrine Cache with overridden doContains method.
 * Fixes a problem with old php_memcache extension.
 */
class MemcacheCache extends BaseMemcacheCache
{
    /**
     * {@inheritdoc}
     */
    protected function doContains($id)
    {
        $memcache = $this->getMemcache();
        if ($memcache->add($id, 0, 0, 1)) {
            $memcache->delete($id);
            return false;
        }
        return true;
    }
}
