<?php

namespace Corporation\CoreBundle\Cache\Adapter;

use Symfony\Bridge\Monolog\Logger;

use Sonata\Cache\CacheElement;
use Sonata\Cache\Adapter\Cache\BaseCacheHandler;

class MemcacheCache extends BaseCacheHandler
{
    protected $servers;

    protected $prefix;

    protected $collection;

    protected $logger;

    /**
     * @param $prefix
     * @param array $servers
     * @param Logger $logger
     */
    public function __construct($prefix, array $servers, Logger $logger)
    {
        $this->prefix  = $prefix;
        $this->servers = $servers;
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function flushAll()
    {
        return $this->getCollection()->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function flush(array $keys = array())
    {
        foreach ($keys as $name => $value) {
            $tagKey = /*md5*/($this->prefix.serialize([$name, $value]));
            $this->clearTag($tagKey);
        }

        return $this->getCollection()->delete($this->computeCacheKeys($keys)) || ! $this->has($keys);
    }

    /**
     * {@inheritdoc}
     */
    public function has(array $keys)
    {
        $flags = null;
        $this->getCollection()->get($this->computeCacheKeys($keys), $flags);

        //if memcache has changed the value of "flags", it means the value exists
        return ($flags !== null);
    }

    /**
     * {@inheritdoc}
     */
    private function getCollection()
    {
        if (!$this->collection) {
            $this->collection = new \Memcache();

            foreach ($this->servers as $server) {
                $this->collection->addServer($server['host'], $server['port'], $server['weight']);
            }
        }

        return $this->collection;
    }

    /**
     * {@inheritdoc}
     */
    public function set(array $keys, $data, $ttl = CacheElement::DAY, array $contextualKeys = array())
    {
        $cacheElement = new CacheElement($keys, $data, $ttl);
        $key = $this->computeCacheKeys($keys);

        foreach ($contextualKeys as $name => $ids) {
            if (!is_array($ids)) {
                $ids = array($ids);
            }

            foreach ($ids as $value) {
                $tagKey = /*md5*/($this->prefix.serialize([$name, $value]));

                if ($this->lockTag($tagKey)) {
                    $tagged = $this->loadTag($tagKey);

                    $tagged[$key] = 1;

                    $this->saveTag($tagKey, $tagged);

                    $this->unlockTag($tagKey);
                }
            }
        }

        $this->getCollection()->set(
            $key,
            $cacheElement,
            0,
            /*
             * The driver does not seems to behave as documented, so we provide a timestamp if the ttl > 30d
             *   http://code.google.com/p/memcached/wiki/NewProgramming#Cache_Invalidation
             */
            $cacheElement->getTtl() + ($cacheElement->getTtl() > 2592000 ? time() : 0)
        );

        return $cacheElement;
    }

    /**
     * {@inheritdoc}
     */
    private function computeCacheKeys(array $keys)
    {
        ksort($keys);

        return md5($this->prefix.serialize($keys));
    }

    /**
     * {@inheritdoc}
     */
    public function get(array $keys)
    {
        try {
            return $this->handleGet($keys, $this->getCollection()->get($this->computeCacheKeys($keys)));
        } catch (\Exception $e) {
            $this->logger->err(json_encode($keys));
            throw $e;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isContextual()
    {
        return true;
    }

    protected function lockTag($tagKey)
    {
        $tries = 0;
        $maxTries = 50;
        $lockTtl = 10;

        $lock = false;

        while ($lock === false && $tries < $maxTries) {
            $lock = $this->getCollection()->add("lockTag_".$tagKey, 1, false, time() + $lockTtl);

            $tries++;

            usleep(mt_rand(3, 10));
        }

        return $lock;
    }

    protected function unlockTag($tagKey)
    {
        return $this->getCollection()->delete("lockTag_".$tagKey);
    }

    protected function loadTag($tagKey)
    {
        $tagged = $this->getCollection()->get("tag_".$tagKey);

        if (!is_array($tagged)) {
            $tagged = array();
        }

        return $tagged;
    }

    protected function saveTag($tagKey, $tagged = false)
    {
        if (!$tagged) {
            return $this->getCollection()->delete("tag_".$tagKey);
        } else {
            return $this->getCollection()->set("tag_".$tagKey, $tagged);
        }

        return false;
    }


    public function clearTag($tagKey)
    {
        if ($this->lockTag($tagKey)) {
            $tagged = $this->loadTag($tagKey);

            foreach ($tagged as $key => $flag) {
                $this->getCollection()->delete($key);
            }

            $this->saveTag($tagKey);

            $this->unlockTag($tagKey);
        }
    }

}
