$(document).ready(function () {
    $(document).on('click', '.btn-beige.load-more-button', function (e) {

        var selectedSearchTerms = getSelectedSearchTerms();
        selectedSearchTerms.offset = $('.posts-container a.image-with-bottom-content:visible').length;

        disableButtons();
        $('body').addClass('ajax-loader');
        $.ajax({
            url: postAjaxUrl,
            data: selectedSearchTerms,
            error: function() {
                // error
                $('.ajax-loader').removeClass('ajax-loader');
            },
            success: function(data) {
                $('<div class="row container-fluid-content' + (($('.more-news-wrapper').length || !$('.featured-post').length) ? '' : ' more-news-wrapper') +'"><div class="news-image-wrapper clearfix posts-container">' + data['html'] + '</div></div>').insertBefore($('.more-news-button-container'));

                $('.ajax-loader').removeClass('ajax-loader');

                if (!data['isMoreButton']) {
                    $('.more-news-button-container .btn-beige.load-more-button').hide();
                } else {
                    $('.more-news-button-container .btn-beige.load-more-button').show();
                }
            },
            complete: function() {
                enableButtons();
            },
            type: 'GET'
        });
    });

    $(document).on('click', '.drop-down-block .dropdown-menu li.dropdown-menu-element a', function (e) {
        e.preventDefault();
        
        var $target = $(e.target);
        var $dropDownToggle = $target.closest('li.drop-element').find('a.dropdown-toggle');
        var $dropDownElement = $target.closest('li.drop-element');

        $dropDownToggle.find('span.filter-term-label').text($target.text());
        $dropDownToggle.attr('data-term-id', $target.data('term-id'));

        var selectedSearchTerms = getSelectedSearchTerms();

        $dropDownElement.removeClass('open');
        $('body').addClass('ajax-loader');
        disableButtons();

        $.ajax({
            url: postAjaxUrl,
            data: selectedSearchTerms,
            error: function() {
                // error
              $('.ajax-loader').removeClass('ajax-loader');
            },
            success: function(data) {
                $('.posts-container:not(:first)').remove();
                $('.posts-container:first').html(data['html']);

                $('.featured-post-container').html(data['featuredHtml']);
                $('.ajax-loader').removeClass('ajax-loader');

                if (!data['isMoreButton']) {
                    $('.more-news-button-container .btn-beige.load-more-button').hide();
                } else {
                    $('.more-news-button-container .btn-beige.load-more-button').show();
                }
            },
            complete: function() {
                enableButtons();
            },
            type: 'GET'
        });
    });

    function getSelectedSearchTerms()
    {
        return {
            'category': $('li.drop-element.categories .dropdown-toggle').attr('data-term-id'),
            'tag': $('li.drop-element.countries .dropdown-toggle').attr('data-term-id')
        };
    }
});

function disableButtons()
{
    $('.more-news-button-container .load-more-button').attr('disabled', 'disabled');
    $('.drop-down-block .drop-element .dropdown-toggle').removeAttr('data-toggle');
    $('.load-more-button').css('cursor', 'default');
}

function enableButtons()
{
    $('.more-news-button-container .load-more-button').removeAttr('disabled');
    $('.drop-down-block .drop-element .dropdown-toggle').attr('data-toggle', 'dropdown');
    $('.load-more-button').css('cursor', 'pointer');
}

