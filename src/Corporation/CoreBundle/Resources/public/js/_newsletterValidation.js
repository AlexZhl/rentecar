$(document).ready(function () {

    var fieldIds = [
        'newsletter_type_email',
        'newsletter_type_firstName',
        'newsletter_type_lastName',
        'newsletter_type_dateOfBirth_day',
        'newsletter_type_dateOfBirth_month',
        'newsletter_type_dateOfBirth_year',
        'newsletter_type_country',
        'newsletter_type_terms'
    ];

    var errorMessages = {
        newsletter_type_email: {
            required: "Email address is required.",
            valid: "Please enter a valid email address.",
            lengthMax: "This value is too long. It should have 254 characters or less."
        },
        newsletter_type_firstName: {
            required: "Please enter a first name.",
            lengthMax: "This value is too long. It should have 64 characters or less.",
            lengthMin: "This value is too short (2 characters minimum required). Please try again."
        },
        newsletter_type_lastName: {
            required: "Please enter a last name.",
            lengthMax: "This value is too long. It should have 64 characters or less.",
            lengthMin: "This value is too short (2 characters minimum required). Please try again."
        },
        newsletter_type_dateOfBirth_day: {
            required: "Please enter a valid date of birth."
        },
        newsletter_type_dateOfBirth_month: {
            required: "Please enter a valid date of birth."
        },
        newsletter_type_dateOfBirth_year: {
            required: "Please enter a valid date of birth."
        },
        newsletter_type_country: {
            required: "Please select a country of residence."
        },
        newsletter_type_terms: {
            required: "Please confirm you accept Terms & Conditions."
        }
    };

    var validateField = function (fieldName, errorMessages) {
        var field = $("#" + fieldName);
        field.trigger('beforeValidation');
        switch (fieldName) {
            case 'newsletter_type_email':
                if (field[0].checkValidity() == false) {
                    var length = field.val().length;
                    if (0 < length) {
                        if (254 < length) {
                            field[0].setCustomValidity(errorMessages[field.attr('id')].lengthMax);
                            addError(field, errorMessages[field.attr('id')].lengthMax);
                        } else {
                            field[0].setCustomValidity(errorMessages[field.attr('id')].valid);
                            addError(field, errorMessages[field.attr('id')].valid);
                        }
                    } else {
                        field[0].setCustomValidity(errorMessages[field.attr('id')].required);
                        addError(field, errorMessages[field.attr('id')].required);
                    }
                }
                break;
            case 'newsletter_type_firstName':
            case 'newsletter_type_lastName':
                if (!field[0].checkValidity()) {
                    var length = field.val().length;
                    if (0 < length) {
                        if (length < 2) {
                            field[0].setCustomValidity(errorMessages[field.attr('id')].lengthMin);
                            addError(field, errorMessages[field.attr('id')].lengthMin);
                        } else if (64 < length) {
                            field[0].setCustomValidity(errorMessages[field.attr('id')].lengthMax);
                            addError(field, errorMessages[field.attr('id')].lengthMax);
                        }
                    } else {
                        field[0].setCustomValidity(errorMessages[field.attr('id')].required);
                        addError(field, errorMessages[field.attr('id')].required);
                    }
                }
                break;
            case 'newsletter_type_dateOfBirth_day':
            case 'newsletter_type_dateOfBirth_month':
            case 'newsletter_type_dateOfBirth_year':
            case 'newsletter_type_country':
                if (!field[0].checkValidity()) {
                    field[0].setCustomValidity(errorMessages[field.attr('id')].required);
                    addError(field, errorMessages[field.attr('id')].required);
                }
                break;
            case 'newsletter_type_terms':
                if (!field[0].checkValidity()) {
                    field[0].setCustomValidity(errorMessages[field.attr('id')].required);
                    addError(field, errorMessages[field.attr('id')].required);
                }

                field.on('invalid', function (e) {
                    e.preventDefault();
                });
                break;
        }
    };

    $('#newsletter_type_email').on('setValueOnPopupShow', function() {
        validateField('newsletter_type_email', errorMessages);
    });

    $('#newsletter_type_email, #newsletter_type_firstName, #newsletter_type_lastName').on('beforeValidation', function() {
        $(this).val($.trim($(this).val()));
    });

    $.each(fieldIds, function (index, value) {
        $("#" + value).on("change", function () {
            $(this)[0].setCustomValidity('');
            removeError($(this));
        });
        $("#" + value).on("blur", function () {
            validateField(value, errorMessages);
        });
    });

    $('#newsletter_type_submit').click(function (e) {
        handleSubmit();
    });

    function handleSubmit() {
        var newsLetterForm = $('#newsletter_type');
        if (newsLetterForm[0].checkValidity()) {
            newsLetterForm.trigger('afterNewsLetterValidation');
            newsLetterForm.submit();
        } else {
            $.each(fieldIds, function (index, value) {
                validateField(value, errorMessages);
            });
        }
    }

    function addError(elem, message) {
        var parent = getParent(elem);

        removeError(elem);
        parent.prepend('<label class="control-label" for="'+ elem.attr('id') +'">' + message + '</label>');
        parent.addClass('has-error');
    }

    function removeError(elem) {
        var parent = getParent(elem);

        parent.removeClass('has-error');
        parent.find('label.control-label').remove();
    }

    function getParent(elem) {
        var parent;
        if (elem.attr('id') == 'newsletter_type_dateOfBirth_day'
                || elem.attr('id') == 'newsletter_type_dateOfBirth_month'
                || elem.attr('id') == 'newsletter_type_dateOfBirth_year'
                || elem.attr('id') == 'newsletter_type_country') {
            parent = elem.parent().parent();
        } else {
            parent = elem.parent();
        }

        return parent;
    }
});