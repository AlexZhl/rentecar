<?php

namespace Corporation\CoreBundle\Settings;

use Sylius\Bundle\SettingsBundle\Schema\SchemaInterface;
use Sylius\Bundle\SettingsBundle\Schema\SettingsBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class AdminSettingsSchema.
 */
class AdminSettingsStageSchema implements SchemaInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * AdminSettingsSchema constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param SettingsBuilderInterface $builder
     */
    public function buildSettings(SettingsBuilderInterface $builder)
    {
        $builder
            ->setDefaults(
                [
                    'current_stage' => '',
                ]
            )
            ->setAllowedTypes(
                [
                    'current_stage' => ['string'],
                ]
            );
    }

    /**
     * @param FormBuilderInterface $builder
     */
    public function buildForm(FormBuilderInterface $builder)
    {
        $settingsManager = $this->container->get('sylius.settings.manager');
        $settings = $settingsManager->loadSettings('stage');

        $builder
            ->add('current_stage')
        ;
    }
}
