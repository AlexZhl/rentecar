<?php

namespace Corporation\CoreBundle\Settings;

use Sonata\AdminBundle\Form\Type\CollectionType;
use Sylius\Bundle\SettingsBundle\Schema\SchemaInterface;
use Sylius\Bundle\SettingsBundle\Schema\SettingsBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class AdminSettingsSchema.
 */
class AdminSettingsSchema implements SchemaInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * AdminSettingsSchema constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param SettingsBuilderInterface $builder
     */
    public function buildSettings(SettingsBuilderInterface $builder)
    {
        $host = $this->container->get('router')->getContext()->getHost();

        $builder
            ->setDefaults(
                [
                    'send_notification_to' => null,
                ]
            )
            ->setAllowedTypes(
                [
                    'send_notification_to' => ['array', 'null'],
                ]
            );
    }

    /**
     * @param FormBuilderInterface $builder
     */
    public function buildForm(FormBuilderInterface $builder)
    {
        $settingsManager = $this->container->get('sylius.settings.manager');

        $builder
            ->add('send_notification_to', CollectionType::class, array(
                'label' => 'settings.send_notification_to',
                'required' => true,
                'allow_add' => true,
                'allow_delete' => true,
                'entry_type' => EmailType::class,
                'entry_options' => array(
                    'required' => true,
                )
            ))
        ;
    }
}
