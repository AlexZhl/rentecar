<?php

namespace Corporation\CoreBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Corporation\CoreBundle\DependencyInjection\Compiler\FormTypeCsrfExtensionPass;
use Corporation\CoreBundle\DependencyInjection\Compiler\CookieSessionHandlerPass;
use Corporation\CoreBundle\DependencyInjection\Compiler\ForcedSslListenerPass;
use Corporation\CoreBundle\DependencyInjection\Compiler\CacheInvalidationPass;

class CorporationCoreBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new FormTypeCsrfExtensionPass());
        $container->addCompilerPass(new CookieSessionHandlerPass());
        $container->addCompilerPass(new ForcedSslListenerPass());
        $container->addCompilerPass(new CacheInvalidationPass());
    }
}
