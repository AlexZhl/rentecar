<?php

namespace Corporation\CoreBundle\DataFixtures\ORM;

use Application\Sonata\PageBundle\Entity\Block;
use Corporation\CoreBundle\Entity\Brand;
use Doctrine\Common\Persistence\ObjectManager;
use PHPExcel_IOFactory;
use Application\Sonata\MediaBundle\Entity\Media;
use Application\Sonata\ClassificationBundle\Entity\Context;
use Application\Sonata\ClassificationBundle\Entity\Category;
use Sonata\PageBundle\Entity\BlockInteractor;
use Sonata\PageBundle\Entity\BlockManager;
use Sonata\PageBundle\Entity\PageManager;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\NullOutput;

class LoadBlocks extends BaseFixture
{
    private $host;

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $this->host = $this->container->get('router')->getContext()->getHost();

        $blockManager = $this->container->get('sonata.page.manager.block');
        $pageManager = $this->container->get('sonata.page.manager.page');
        $blockInteractor = $this->container->get('sonata.page.block_interactor');

        $ob = simplexml_load_file($this->getImportFile('blocks-data'), null, LIBXML_NOCDATA);
        $json = json_encode($ob);
        $pagesData = json_decode($json, true);

        foreach ($pagesData as $pageData) {
            $where = [];
            if ($pageData['routeName']) {
                $where['routeName'] = $pageData['routeName'];
            }
            if (isset($pageData['url']) && $pageData['url']) {
                $where['url'] = $pageData['url'];
            }
            $page = $pageManager->findOneBy($where);

            if (isset($pageData['title']) && $pageData['title']) {
                $page->setTitle($pageData['title']);
            }
            if (isset($pageData['metaDescription']) && $pageData['metaDescription']) {
                $page->setMetaDescription($pageData['metaDescription']);
            }
            if (isset($pageData['metaKeyword']) && $pageData['metaKeyword']) {
                $page->setMetaKeyword($pageData['metaKeyword']);
            }
            if (isset($pageData['template']) && $pageData['template']) {
                $page->setTemplateCode($pageData['template']);
            }

            $containers = $pageData['containers'];
            foreach ($containers as $containerCode => $containerData) {
                $content = $blockInteractor->createNewContainer(array(
                    'enabled' => true,
                    'page' => $page,
                    'code' => $containerCode,
                ));

                $page->addBlocks($content);
                $content->setName($containerData['name']);

                foreach ($containerData['blocks'] as &$blockDataRef) {
                    if (isset($blockDataRef['imageFileName'])) {
                        $media = new Media();
                        $media->setProviderName('sonata.media.provider.image');
                        $media->setContext(Context::BANNER);
                        $media->setBinaryContent($this->getImportFile('BannerImages') . $blockDataRef['imageFileName']);

                        $blockDataRef['mediaEntity'] = $media;

                        $manager->persist($media);
                    }
                }

                $manager->flush();

                foreach ($containerData['blocks'] as $blockData) {
                    /** @var Block $block */
                    $block = $blockManager->create();
                    $block->setPage($page);
                    $block->setName($blockData['name']);
                    $block->setType($blockData['type']);
                    $block->setSettings($blockData['settings']);
                    if (isset($blockData['mediaEntity'])) {
                        $block->setSetting('mediaId', $blockData['mediaEntity']->getId());
                    }
                    $block->setPosition($blockData['position']);
                    $block->setEnabled(true);

                    $content->addChildren($block);
                }
            }

            $pageManager->save($page);
        }

        $kernel = $this->container->get('kernel');
        $application = new Application($kernel);
        $application->setAutoExit(false);
        $input = new StringInput('sonata:page:create-snapshots --site=all');
        $output = new NullOutput();
        $application->run($input, $output);
    }

    public function setHost($blockContent)
    {
        if (isset($blockContent["content"])) {
            preg_match('%HOST%', $blockContent["content"], $matches);
            if (isset($matches)) {
                $blockContent["content"] = str_replace('%HOST%', $this->host, $blockContent["content"]);
            }
        }

        return $blockContent;
    }
}
