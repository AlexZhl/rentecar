<?php

namespace Corporation\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Class BaseFixture.
 */
class BaseFixture extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    /**
     * @var array
     */
    private $order = array(
        'BaseFixture',
        'LoadTypes',
        'LoadAdmins',
        'LoadPages',
        'LoadSettings',
        'LoadMenuItems',
        'LoadBlocks',
        'LoadPosts'
    );
    
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var string
     */
    private $distImportDir = '';

    /**
     * @var array
     */
    protected $importFiles = [
        'blocks-data' => '/Resources/import/ContentXLSX/blocks-data.xml',
        'content-data' => '/Resources/import/ContentXLSX/content-data.xml',
        'pages-data' => '/Resources/import/ContentXLSX/pages-data.xml',
    ];

    /**
     * @param $key
     * @return string
     */
    protected function getImportFile($key)
    {
        return $this->container->getParameter('kernel.root_dir') . $this->importFiles[$key];
    }

    /**
     * Sets container.
     *
     * @param ContainerInterface $container Container instanase.
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        $className = substr(get_class($this), strrpos(get_class($this), '\\') + 1);

        return array_search($className, $this->order);
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {

    }

    /**
     * Get data from Yaml file.
     *
     * @param string $filename
     *
     * @return array
     */
    protected function getYamlData($filename)
    {
        $file = __DIR__.
            DIRECTORY_SEPARATOR.'..'.
            DIRECTORY_SEPARATOR.'..'.
            DIRECTORY_SEPARATOR.'Resources'.
            DIRECTORY_SEPARATOR.'fixtures'.
            DIRECTORY_SEPARATOR.$filename;

        return Yaml::parse(file_get_contents($file));
    }
}
