<?php

namespace Corporation\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\NullOutput;

/**
 * Class LoadPages.
 */
class LoadPages extends BaseFixture
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $kernel = $this->container->get('kernel');

        $application = new Application($kernel);
        $application->setAutoExit(false);

        $baseHost = $this->container->getParameter('base_host');
        $input = new StringInput(
            sprintf(
                'sonata:page:create-site --no-confirmation=true --enabled=true --name=localhost --locale=- --host=%s --relativePath=/ --enabledFrom=now --enabledTo="+10 years" --default=true',
                str_replace(['http://', 'https://'], '', $baseHost)
            )
        );

        $output = new NullOutput();
        $application->run($input, $output);

        $site = $this->container->get('sonata.page.manager.site')->findOneBy(['host' => $baseHost]);
        $this->setReference('site', $site);

        $sonataPageManager = $this->container->get('sonata.page.manager.page');

        $pagesDataXml = simplexml_load_file($this->getImportFile('pages-data'), null, LIBXML_NOCDATA);
        $pagesData = json_decode(json_encode($pagesDataXml), true);

        foreach ($pagesData as $pageData) {
            $page = $this->container->get('sonata.page.manager.page')->create(array(
                'routeName' => $pageData['routeName'],
                'site' => $site,
            ));
            $this->setPageDataSafe($page, $pageData);
            $sonataPageManager->save($page);

            foreach ($pageData['children'] as $childData) {
                $childPage = $this->container->get('sonata.page.manager.page')->create(array(
                    'routeName' => $childData['routeName'],
                    'site' => $site,
                ));
                $this->setPageDataSafe($childPage, $childData);
                $childPage->setParent($page);
                $sonataPageManager->save($childPage);
            }
        }

        $input = new StringInput('sonata:page:update-core-routes --site=all');
        $application->run($input, $output);

        $input = new StringInput('sonata:page:create-snapshots --site=all');
        $application->run($input, $output);

        $template = $this->container->get('sonata.page.manager.page')->create(array(
            'routeName' => '_page_internal_template',
            'name' => '_page_internal_template',
            'templateCode' => 'default',
            'url' => null,
            'site' => $site,
            'requestMethod' => null,
            'slug' => null,
            'decorate' => false,
            'enabled' => true,
            'edited' => false,
        ));

        $this->container->get('sonata.page.manager.page')->save($template);

    }

    private function setPageDataSafe($page, $pageData)
    {
        if (isset($pageData['name']) && $pageData['name']) {
            $page->setName($pageData['name']);
        }
        if (isset($pageData['templateCode']) && $pageData['templateCode']) {
            $page->setTemplateCode($pageData['templateCode']);
        }
        if (isset($pageData['url']) && $pageData['url']) {
            $page->setUrl($pageData['url']);
        }
        if (isset($pageData['requestMethod']) && $pageData['requestMethod']) {
            $page->setRequestMethod($pageData['requestMethod']);
        }
        if (isset($pageData['slug']) && $pageData['slug']) {
            $page->setSlug($pageData['slug']);
        }
        if (isset($pageData['enabled']) && $pageData['enabled']) {
            $page->setEnabled($pageData['enabled']);
        }
    }
}
