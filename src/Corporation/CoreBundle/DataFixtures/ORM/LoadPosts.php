<?php

namespace Corporation\CoreBundle\DataFixtures\ORM;

use Application\Sonata\ClassificationBundle\Entity\Context;
use Application\Sonata\NewsBundle\Entity\NewsPost;
use Application\Sonata\NewsBundle\Entity\HistoryPost;
use Corporation\CoreBundle\Entity\Car;
use Corporation\CoreBundle\Entity\Testimonial;
use Doctrine\Common\Persistence\ObjectManager;
use Application\Sonata\MediaBundle\Entity\Media;

class LoadPosts extends BaseFixture
{

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $manager->clear();

        $ob = simplexml_load_file($this->getImportFile('content-data'), null, LIBXML_NOCDATA);
        $json = json_encode($ob);
        $pagesData = json_decode($json, true);

        foreach ($pagesData['cars']['car'] as $carData) {
            $car = new Car();
            // Main info
            $car->setMake($carData['make']);
            $car->setModel($carData['model']);
            $car->setEngine($carData['engine']);
            $car->setTransmission($carData['transmission']);
            $car->setYear($carData['year']);
            $car->setColor($carData['color']);
            $car->setAdditionalOptions($carData['additionalOptions']['option']);
            $car->setDescription($carData['description']);
            $car->setPricePerDay($carData['pricePerDay']);
            $car->setDisplayOnMainPage($carData['displayOnMainPage']);

            // Photos
            $media = new Media();
            $media->setProviderName('sonata.media.provider.image');
            $media->setContext(Context::CAR);
            $media->setBinaryContent('app/Resources/import/FixtureImages/' . $carData['mainPhotoFileName']);
            $manager->persist($media);
            $car->setMainPhoto($media);
            foreach ($carData['photos']['photo'] as $additionalPhoto) {
                $media = new Media();
                $media->setProviderName('sonata.media.provider.image');
                $media->setContext(Context::CAR);
                $media->setBinaryContent('app/Resources/import/FixtureImages/' . $additionalPhoto);
                $manager->persist($media);

                $car->addPhoto($media);
            }

            // SEO
            $car->setSlug($carData['slug']);
            $car->setSeoTitle($carData['seoTitle']);
            $car->setSeoDescription($carData['seoDescription']);
            $car->setSeoKeywords($carData['seoKeywords']);

            $manager->persist($car);
        }

        foreach ($pagesData['testimonials']['testimonial'] as $testimonialData) {
            $testimonial = new Testimonial();
            $testimonial->setName($testimonialData['name']);
            $testimonial->setText($testimonialData['text']);
            $testimonial->setApproved($testimonialData['approved']);

            $manager->persist($testimonial);
        }


//        $media = new Media();
//        $media->setProviderName('sonata.media.provider.image');
//        $media->setContext('article');
//        $media->setBinaryContent('app/Resources/public/images/FixtureImages/post.jpg');
//        $manager->persist($media);
//
//        foreach ($pagesData['news']['article'] as $news) {
//            $newsArticle = new NewsPost();
//            $newsArticle->setTitle($news['title']);
//            $newsArticle->setAbstract('abstract');
//            $newsArticle->setCategories([$this->getReference('article_category')]);
//            $newsArticle->setImage($media);
//            $newsArticle->setRawContent($news['content']);
//            $newsArticle->setContent($news['content']);
//            $newsArticle->setEnabled(true);
//            $newsArticle->setContentFormatter('richhtml');
//            $newsArticle->setCommentsDefaultStatus('0');
//            $newsArticle->setCommentsEnabled(false);
//            $newsArticle->setCommentsEnabled(false);
//            $newsArticle->setSubTitle($news['subtitle']);
//            $manager->persist($newsArticle);
//        }
//
        $manager->flush();
    }
}
