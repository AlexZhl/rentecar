<?php

namespace Corporation\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadSettings.
 */
class LoadSettings extends BaseFixture
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $settingsManager = $this->container->get('sylius.settings.manager');
        $settings = $settingsManager->loadSettings('default');

        $settings->set('send_notification_to', null);

        $settingsManager->saveSettings('default', $settings);
    }
}
