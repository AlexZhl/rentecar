<?php
namespace Corporation\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Application\Sonata\NewsBundle\Entity\People;

class LoadMenuItems extends BaseFixture implements FixtureInterface
{
    public function load(ObjectManager $objectManager)
    {
        $factory = $this->container->get('corporation_menu.factory');

        $menu = $factory->createItem('main', array(
                'childrenAttributes' => array('class' => 'nav navbar-nav'),
            )
        );

        $menu->addChild('Главная', array(
                'link' => 'corporation_index',
                'type' => 'route',
                'extras' => array(),
                'attributes' => array('class' => 'nav-item'),
                'labelAttributes' => array(),
                'linkAttributes' => array('class' => 'nav-link'),
                'childrenAttributes' => array(),
            )
        );

        $menu->addChild('Автомобили', array(
                'link' => 'corporation_cars',
                'type' => 'route',
                'extras' => array(),
                'attributes' => array('class' => 'nav-item'),
                'labelAttributes' => array(),
                'linkAttributes' => array('class' => 'nav-link'),
                'childrenAttributes' => array(),
            )
        );

        $menu->addChild('Отзывы', array(
                'link' => 'corporation_feedback',
                'type' => 'route',
                'extras' => array(),
                'attributes' => array('class' => 'nav-item'),
                'labelAttributes' => array(),
                'linkAttributes' => array('class' => 'nav-link'),
                'childrenAttributes' => array(),
            )
        );

        $menu->addChild('Контакты', array(
                'link' => 'corporation_contact',
                'type' => 'route',
                'extras' => array(),
                'attributes' => array('class' => 'nav-item'),
                'labelAttributes' => array(),
                'linkAttributes' => array('class' => 'nav-link'),
                'childrenAttributes' => array(),
            )
        );

        $objectManager->persist($menu);

        $objectManager->flush();
    }
}