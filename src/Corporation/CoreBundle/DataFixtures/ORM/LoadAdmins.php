<?php

namespace Corporation\CoreBundle\DataFixtures\ORM;

use Application\Sonata\UserBundle\Entity\Admin;
use Doctrine\Common\Persistence\ObjectManager;

class LoadAdmins extends BaseFixture
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        /** @var Admin $admin */
        $admin = new Admin();
        $admin->setFirstName('John');
        $admin->setLastname('Smith');
        $admin->setPlainPassword('12345678');
        $admin->setUsername('admin@test.com');
        $admin->setUsernameCanonical('admin@test.com');
        $admin->setEmail('admin@test.com');
        $admin->setEmailCanonical('admin@test.com');
        if ($this->container->has('sonata.user.google.authenticator.provider')) {
            $admin->setTwoStepVerificationCode(
                $this->container->get('sonata.user.google.authenticator.provider')->generateSecret()
            );
        }
        $admin->setRoles(['ROLE_SUPER_ADMIN']);
        $admin->setEnabled(true);

        $manager->persist($admin);
        $manager->flush();
    }
}
