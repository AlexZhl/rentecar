<?php

namespace Corporation\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Application\Sonata\ClassificationBundle\Entity\Context;
use Application\Sonata\ClassificationBundle\Entity\Category;

class LoadTypes extends BaseFixture
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $context = new Context();
        $context->setId('car');
        $context->setName('car');
        $context->setEnabled(true);
        $manager->persist($context);

        $category = new Category();
        $category->setName('Media');
        $category->setEnabled(true);
        $category->setSlug('car');
        $category->setContext($context);
        $manager->persist($category);
        $this->setReference("car_category", $category);


        $context = new Context();
        $context->setId('article');
        $context->setName('article');
        $context->setEnabled(true);
        $manager->persist($context);

        $category = new Category();
        $category->setName('Media');
        $category->setEnabled(true);
        $category->setSlug('article');
        $category->setContext($context);
        $manager->persist($category);
        $this->setReference("article_category", $category);

        $manager->flush();

        $manager->flush();
    }
}

