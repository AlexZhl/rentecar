<?php

namespace Corporation\CoreBundle\Security;

use Symfony\Component\Security\Core\Exception\LockedException;
use Symfony\Component\Security\Core\Exception\DisabledException;
use Symfony\Component\Security\Core\Exception\AccountExpiredException;
use Symfony\Component\Security\Core\User\UserChecker as BaseUserChecker;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

class UserChecker extends BaseUserChecker
{
    private $translator;
    private $em;
    private $router;

    public function __construct($translator, $em, $router)
    {
        $this->translator = $translator;
        $this->em = $em;
        $this->router = $router;
    }
    /**
     * {@inheritdoc}
     */
    public function checkPreAuth(UserInterface $user)
    {
        $currentDate = new \DateTime('now');

        if (!$user instanceof AdvancedUserInterface) {
            return;
        }

        if (!$user->isAccountNonLocked()) {
            $lockedUntil = $user->getLockedUntil();
            if ($lockedUntil) {
                if ($currentDate->getTimestamp() > $lockedUntil->getTimestamp()) {
                    $user->setLocked(false);
                    $user->setLockedUntil(null);
                    $user->setLoginFailuresCount(null);
                } else {
                    $ex = new LockedException($this->translator->trans('account_locked'));
                    $ex->setUser($user);
                    throw $ex;
                }
            }
        }

        if (!$user->isEnabled()) {
            $ex = new DisabledException('User account is disabled.');
            $ex->setUser($user);
            throw $ex;
        }

    }

    public function checkPostAuth(UserInterface $user)
    {
        if (!$user->isCredentialsNonExpired()) {
            $url = $this->router->generate('sonata_user_admin_resetting_request');
            $ex = new AccountExpiredException($this->translator->trans('account_expired', ['%path%' => $url]));
            $ex->setUser($user);
            throw $ex;
        }
    }
}