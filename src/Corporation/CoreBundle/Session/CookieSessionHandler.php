<?php

namespace Corporation\CoreBundle\Session;

use Application\Sonata\UserBundle\Entity\UserInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Psr\Log\LoggerInterface;

use Nelmio\SecurityBundle\Session\CookieSessionHandler as BaseCookieSessionHandler;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Role\Role;

class CookieSessionHandler extends BaseCookieSessionHandler
{
    protected $roleHierarchy;

    protected $roleCookieLifeTime;
    /**
     * @param FilterResponseEvent $e
     */
    public function onKernelResponse(FilterResponseEvent $e)
    {
        if (HttpKernelInterface::MASTER_REQUEST !== $e->getRequestType()) {
            return;
        }

        if (!$this->request) {
            if ($this->logger) {
                $this->logger->crit('CookieSessionHandler::read - The Request object is missing');
            }

            return;
        }

        if ($this->logger) {
            $this->logger->debug('CookieSessionHandler::onKernelResponse - Get the Response object');
        }

        $this->request->getSession()->save();

        if ($this->cookie === false) {
            if ($this->logger) {
                $this->logger->debug('CookieSessionHandler::onKernelResponse - COOKIE not opened');
            }

            return;
        }

        if ($this->cookie === null) {
            if ($this->logger) {
                $this->logger->debug('CookieSessionHandler::onKernelResponse - CLEAR COOKIE');
            }
            $e->getResponse()->headers->clearCookie($this->cookieName);
        } else {
            $e->getResponse()->headers->setCookie($this->cookie);
        }
    }


    /**
     * {@inheritdoc}
     */
    public function write($sessionId, $sessionData)
    {
        if ($this->logger) {
            $this->logger->debug(sprintf('CookieSessionHandler::write sessionId=%s', $sessionId));
        }

        /**
         * @var Session $session
         */
        $session = $this->request->getSession();

        $lifetime = $this->lifetime;

        /**
         * @var UsernamePasswordToken $token
         */
        $roles = [];
        if ($session->get('_security_admin')) {
            $token = unserialize($session->get('_security_admin'));
            if ($token) {
                $roles = $token->getRoles();
            }
        } else {
            $roles[] = new Role('IS_AUTHENTICATED_ANONYMOUSLY');
        }
        if ($roles) {
            $lifetime = $this->lookupRoleLifetime($roles);
        }

        $expire = $lifetime === 0 ? 0 : strtotime('now') + $lifetime;

        $this->cookie = new Cookie(
            $this->cookieName,
            serialize(array('expire' => $expire, 'data' => $sessionData)),
            $expire,
            $this->path,
            $this->domain,
            $this->secure,
            $this->httpOnly
        );

        return true;
    }

    public function setRoleHierarchy($roles)
    {
        $this->roleHierarchy = $roles;
    }

    public function setCookieLifeTimeRoles($roleCookieLifeTime)
    {
        $this->roleCookieLifeTime = $roleCookieLifeTime;
    }


    /**
     * lookup cookie lifetime
     *
     * @param Role[] $roles
     */
    protected function lookupRoleLifetime($roles)
    {
        /**
         * fill temporary unique user roles
         * @var Role[] $tmpRoles
         */
        $tmpRoles = [];
        $hierarchy = $this->roleHierarchy;

        $isNestedInTmp = function (Role $role) use ($hierarchy, $tmpRoles) {
            foreach ($tmpRoles as $tmpRole) {
                if (!array_key_exists($tmpRole->getRole(), $hierarchy)) {
                    continue;
                }

                if (in_array($role->getRole(), $hierarchy[$tmpRole->getRole()])) {
                    return true;
                }
            }
            return false;
        };

        /**
         * iterate each user's role
         */
        while ($role = array_shift($roles)) {
            /**
             * if role is hierarchical (eg ROLE_USER: [ROLE_MANAGER, ROLE_WRITER])
             * we should remove child roles
             */
            if (array_key_exists($role->getRole(), $hierarchy)) {
                foreach ($roles as $key => $nestedRole) {
                    if (in_array($nestedRole->getRole(), $hierarchy[$role->getRole()])) {
                        unset($roles[$key]);
                    }
                }
            }
            /**
             * check is iterated role in hierarchy on some role in temporary role storage($tmpRoles)
             * if not add to temp storage
             */
            if (!$isNestedInTmp($role)) {
                $tmpRoles[] = $role;
            }
        }

        $lifeTimeRoles = [];

        /**
         * iterate role over temporary roles storage and if role exist in lifetime role config
         * fill array $lifeTimeRoles lifetime values
         */
        foreach ($tmpRoles as $tmpRole) {
            if (array_key_exists($tmpRole->getRole(), $this->roleCookieLifeTime)) {
                $lifeTimeRoles[$tmpRole->getRole()] = $this->roleCookieLifeTime[$tmpRole->getRole()];
            }
        }

        /**
         * if $lifeTimeRoles not empty lookup min lifetime value
         * otherwise return default
         */
        if ($lifeTimeRoles) {
            $minLifetime = min($lifeTimeRoles);
            if ($this->logger) {
                $key = array_search($minLifetime, $lifeTimeRoles);
                $this->logger->info('Cookie lifetime in ' . $key . ' = ' . $minLifetime);
            }
        } else {
            $this->logger->info('Cookie lifetime is default = ' . $this->lifetime);
            $minLifetime = $this->lifetime;
        }

        return $minLifetime;
    }
}
