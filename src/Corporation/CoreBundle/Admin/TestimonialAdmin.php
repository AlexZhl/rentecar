<?php

namespace Corporation\CoreBundle\Admin;

use Corporation\CoreBundle\Entity\Testimonial;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class TestimonialAdmin extends Admin
{
    protected $baseRouteName = 'testimonial';
    protected $baseRoutePattern = 'testimonial';
    protected $translationDomain = 'TestimonialAdmin';

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('approved')
            ->add('createdAt')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id', 'text', ['disabled' => true])
            ->add('name', 'text', ['required' => false])
            ->add('text', 'textarea')
            ->add('approved')
            ->add('createdAt', 'sonata_type_datetime_picker')
        ;
    }
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('name')
            ->addIdentifier('approved')
            ->addIdentifier('createdAt')
        ;
    }

    public function toString($object)
    {
        return $object instanceof Testimonial
            ? $object->getName()
            : 'Testimonial';
    }
}
