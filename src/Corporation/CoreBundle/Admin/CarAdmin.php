<?php

namespace Corporation\CoreBundle\Admin;

use Corporation\CoreBundle\Entity\Car;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use \Sonata\AdminBundle\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CarAdmin extends Admin
{
    protected $baseRouteName = 'car';
    protected $baseRoutePattern = 'car';
    protected $translationDomain = 'CarAdmin';

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('make')
            ->add('model')
            ->add('year')
            ->add('transmission')
            ->add('pricePerDay')
            ->add('createdAt')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Основная информация', array(
                'class' => 'col-md-6'
            ))
            ->add('id', 'text', ['disabled' => true])
            ->add('make', 'text')
            ->add('model', 'text')
            ->add('engine', 'text')
            ->add('transmission', 'text')
            ->add('year', 'text')
            ->add('color', 'sonata_type_color_selector')
            ->add('additionalOptions', CollectionType::class, array(
                'required' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'entry_type' => TextType::class,
                'entry_options' => array(
                    'required' => false
                )
            ))
            ->add('description', 'textarea')
            ->add('pricePerDay', 'text')
            ->add('displayOnMainPage')
            ->end()

            ->with('Фотографии', array(
                'class' => 'col-md-6'
            ))
            ->add('mainPhoto')
            ->add('photos')
            ->end()

            ->with('SEO', array(
                'class' => 'col-md-6'
            ))
            ->add('slug', 'text')
            ->add('seoTitle', 'text')
            ->add('seoDescription', 'text')
            ->add('seoKeywords', 'text')
            ->end()
        ;
    }
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('make')
            ->addIdentifier('model')
            ->addIdentifier('pricePerDay')
            ->addIdentifier('displayOnMainPage')
        ;
    }

    public function toString($object)
    {
        if ($object instanceof Car) {
            return $object->getMake() ? : 'Car';
        }

        return $object instanceof Car
            ? $object->getMake()
            : 'Car';
    }
}
