<?php

namespace Corporation\CoreBundle\Admin;

use Corporation\CoreBundle\Entity\ServiceRequest;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class ServiceRequestAdmin extends Admin
{
    protected $baseRouteName = 'service_request';
    protected $baseRoutePattern = 'service_request';
    protected $translationDomain = 'ServiceRequestAdmin';

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('phone')
            ->add('email')
            ->add('message')
            ->add('createdAt')
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('edit');
        $collection->remove('create');
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id', 'text', ['disabled' => true])
            ->add('name', 'text', ['required' => false])
            ->add('phone')
            ->add('email', 'text', ['required' => false])
            ->add('message', 'textarea', ['required' => false])
        ;

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('name')
            ->addIdentifier('phone')
            ->addIdentifier('createdAt')
        ;
    }

    public function toString($object)
    {
        return $object instanceof ServiceRequest
            ? $object->getName()
            : 'ServiceRequest';
    }
}
