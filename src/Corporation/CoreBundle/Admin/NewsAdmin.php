<?php

namespace Corporation\CoreBundle\Admin;

use Application\Sonata\ClassificationBundle\Entity\Tag;
use Application\Sonata\ClassificationBundle\Entity\Category;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Application\Sonata\ClassificationBundle\Entity\Context;
use Sonata\CoreBundle\Validator\ErrorElement;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class NewsAdmin extends PostAdmin
{
    protected $baseRouteName = 'news';
    protected $baseRoutePattern = 'news';
    protected $translationDomain = 'NewsAdmin';

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine.orm.entity_manager');
        $formMapper
            ->with('General Info', array(
                'class' => 'col-md-8'
            ))
            ->add('title')
            ->add('subTitle')
            ->add('image', 'sonata_type_model_list', array('required' => true), array(
                'link_parameters' => array(
                    'context' => 'article'
                )
            ))
            ->add('abstract', 'sonata_simple_formatter_type', array(
                'format' => 'richhtml',
                'ckeditor_context' => 'news',
                'required' => false
            ))
            ->add('rawContent', 'sonata_simple_formatter_type', array(
                'format' => 'richhtml',
                'ckeditor_context' => 'news',
            ))
            ->end()
            ->with('Status', array(
                'class' => 'col-md-4'
            ));

        $formMapper
            ->add('enabled', null, array('required' => false))
            ->end()
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine.orm.entity_manager');
        $datagridMapper
            ->add('title')
//            ->add('categories', 'doctrine_orm_callback', array(
//                    'callback' => function($queryBuilder, $alias, $field, $value) use ($em) {
//                        if (!$value['value']) {
//                            return;
//                        }
//
//                        $queryBuilder
//                            ->leftJoin($alias . '.categories', 'c')
//                            ->andWhere('c.context = :context')
//                            ->andWhere('c.slug = :slug')
//                            ->setParameter('context', Context::NEWS)
//                            ->setParameter('slug', $value['value']);
//
//                        return true;
//                    },
//                    'field_type' => 'choice',
//                    'field_options' => array(
//                        'choices' => $em->getRepository('ApplicationSonataClassificationBundle:Category')->getCategoriesByContext(Context::NEWS),
//                    )
//                )
//            )
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
//            ->add('categories', null, array('route' => array('name' => '')))
            ->add('createdAt', 'datetime')
            ->add('enabled')
        ;
    }

    /**
     * @param \Sonata\CoreBundle\Validator\ErrorElement $errorElement
     * @param  $object
     * @return void
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        if (!$object->getImage()) {
            $errorElement
                    ->with('image')
                    ->assertNotNull()
                    ->end();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function prePersist($post)
    {
        parent::prePersist($post);
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine.orm.entity_manager');
        $post->setCollection($em->getRepository('ApplicationSonataClassificationBundle:Type')->findOneBy(array('slug' => 'news')));
    }

}
