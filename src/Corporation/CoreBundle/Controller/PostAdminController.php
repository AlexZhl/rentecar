<?php

namespace Corporation\CoreBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sonata\AdminBundle\Exception\ModelManagerException;

use Pix\SortableBehaviorBundle\Services\PositionHandler;

class PostAdminController extends CRUDController
{

    /**
     * Execute a batch featured.
     *
     * @param ProxyQueryInterface $query
     * @param Request             $request
     *
     * @return RedirectResponse
     *
     * @throws AccessDeniedException If access is not granted
     */
    public function batchActionFeatured(ProxyQueryInterface $query, Request $request = null)
    {
        if (!$this->admin->isGranted('EDIT')) {
            throw new AccessDeniedException();
        }

        //$request = $this->resolveRequest($request);

        $modelManager = $this->admin->getModelManager();
        try {
            $posts = $query->execute();
            foreach ($posts as $post) {
                $post->setFeatured(true);
                $modelManager->update($post);
            }
            $this->addFlash('sonata_flash_success', $this->get('translator')->trans('flash_batch_featured_success', array(), 'NewsAdmin'));
        } catch (ModelManagerException $e) {
            $this->handleModelManagerException($e);
            $this->addFlash('sonata_flash_error', $this->get('translator')->trans('flash_batch_featured_error', array(), 'NewsAdmin'));
        }

        return new RedirectResponse($this->admin->generateUrl(
            'list', array('filter' => $this->admin->getFilterParameters())
        ));
    }

    /**
     * Execute a batch enabled.
     *
     * @param ProxyQueryInterface $query
     * @param Request             $request
     *
     * @return RedirectResponse
     *
     * @throws AccessDeniedException If access is not granted
     */
    public function batchActionEnabled(ProxyQueryInterface $query, Request $request = null)
    {
        if (!$this->admin->isGranted('EDIT')) {
            throw new AccessDeniedException();
        }

        //$request = $this->resolveRequest($request);

        $modelManager = $this->admin->getModelManager();
        try {
            $posts = $query->execute();
            foreach ($posts as $post) {
                $post->setEnabled(true);
                $modelManager->update($post);
            }
            $this->addFlash('sonata_flash_success', $this->get('translator')->trans('flash_batch_enabled_success', array(), 'NewsAdmin'));
        } catch (ModelManagerException $e) {
            $this->handleModelManagerException($e);
            $this->addFlash('sonata_flash_error', $this->get('translator')->trans('flash_batch_enabled_error', array(), 'NewsAdmin'));
        }

        return new RedirectResponse($this->admin->generateUrl(
            'list', array('filter' => $this->admin->getFilterParameters())
        ));
    }

    public function moveAction($position)
    {
        $translator = $this->get('translator');

        if (!$this->admin->isGranted('EDIT')) {
            $this->addFlash(
                'sonata_flash_error',
                $translator->trans('flash_error_no_rights_update_position')
            );

            return new RedirectResponse($this->admin->generateUrl('list', $this->admin->getFilterParameters()));
        }

        $object = $this->admin->getSubject();

        /** @var PositionHandler $positionService */
        $positionService = $this->get('pix_sortable_behavior.position');

        $entity = \Doctrine\Common\Util\ClassUtils::getClass($object);

        $lastPosition = $positionService->getLastPosition($entity);

        $position = $positionService->getPosition($object, $position, $lastPosition);

        $setter = sprintf('set%s', ucfirst($positionService->getPositionFieldByEntity($entity)));

        if (!method_exists($object, $setter)) {
            throw new \LogicException(
                sprintf(
                    '%s does not implement ->%s() to set the desired position.',
                    $object,
                    $setter
                )
            );
        }

        $object->{$setter}($position);
        $this->admin->update($object);

        if ($this->isXmlHttpRequest()) {
            return $this->renderJson(array(
                'result' => 'ok',
                'objectId' => $this->admin->getNormalizedIdentifier($object)
            ));
        }

        $this->addFlash(
            'sonata_flash_success',
            $translator->trans('flash_success_position_updated')
        );

        return new RedirectResponse($this->admin->generateUrl(
            'list',
            array('filter' => $this->admin->getFilterParameters())
        ));
    }
}
