<?php

namespace Corporation\CoreBundle\Controller;

use Corporation\CoreBundle\Entity\Car;
use Corporation\MenuBundle\Entity\Repository\MenuRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/")
 */
class CarsController extends Controller
{
    /**
     * @Route("/cars", methods={"GET", "POST"}, name="corporation_cars")
     */
    public function carsAction(Request $request, $isMainPage = false)
    {
        $page = $request->request->getInt('page', 1);
        $itemsPerPage = $request->request->getInt('page', Car::ITEMS_PER_PAGE);

        return $this->render('@CorporationCore/Cars/index.html.twig', [
            'pagination' => $this->get('corporation_core.manager.car')->getCarsWithPagination($page, $itemsPerPage, $isMainPage),
        ]);
    }

    /**
     * @Route("/cars/{slug}", methods={"GET", "POST"}, name="corporation_car")
     */
    public function carAction(Request $request, $slug = '')
    {
        $carManager = $this->get('corporation_core.manager.car');

        $car = $carManager->getCar($slug);
        if (!$car)  {
            throw new NotFoundHttpException();
        }

        $seoPage = $this->get('sonata.seo.page');
        $seoPage
            ->setTitle($car->getSeoTitle())
            ->addMeta('name', 'description', $car->getSeoDescription())
            ->addMeta('name', 'keywords', $car->getSeoKeywords())
        ;

        return $this->render('@CorporationCore/Cars/details.html.twig', [
            'car' => $car,
        ]);
    }
}
