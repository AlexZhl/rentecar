<?php

namespace Corporation\CoreBundle\Controller;

use Corporation\MenuBundle\Entity\Repository\MenuRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", methods={"GET", "POST"}, name="corporation_index")
     */
    public function indexAction()
    {
        return $this->render('@CorporationCore/Home/index.html.twig', [
        ]);
    }

    /**
     * @Route("/services", methods={"GET", "POST"}, name="corporation_services")
     */
    public function servicesAction()
    {
        return $this->render('@CorporationCore/Services/index.html.twig', [
        ]);
    }

    /**
     * @Route("/pricelist", methods={"GET", "POST"}, name="corporation_prices")
     */
    public function pricesAction(Request $request)
    {
        return $this->render('@CorporationCore/Prices/index.html.twig', [
        ]);
    }

    /**
     * @Route("/contacts", methods={"GET", "POST"}, name="corporation_contact")
     */
    public function contactsAction(Request $request)
    {
        return $this->render('@CorporationCore/Contact/index.html.twig', [
        ]);
    }

    /**
     * @Route("/team", methods={"GET", "POST"}, name="corporation_team")
     */
    public function teamAction(Request $request)
    {
        if (!$this->get('corporation_menu.menu_item_repository')->getMenuItemByName('Врачи')->isEnabled()) {
            throw new NotFoundHttpException();
        }

        return $this->render('@CorporationCore/Team/index.html.twig', [
        ]);
    }
}
