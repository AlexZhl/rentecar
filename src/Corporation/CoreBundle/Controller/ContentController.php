<?php

namespace Corporation\CoreBundle\Controller;

use Corporation\CoreBundle\Entity\ServiceRequest;
use Corporation\CoreBundle\Entity\Testimonial;
use Corporation\CoreBundle\Form\Type\ServiceRequestType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Corporation\CoreBundle\Form\Type\TestimonialType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/")
 */
class ContentController extends Controller
{
    /**
     * @Route("/feedback", methods={"GET", "POST"}, name="corporation_feedback")
     */
    public function feedbackAction(Request $request)
    {
        $testimonial = new Testimonial();
        $form = $this->createForm(TestimonialType::class, $testimonial);

        $response = null;

        if ($request->isMethod('POST')) {
            $form->submit($request);
            $form->getData();
            if ($form->isValid()) {
                $this->get('corporation_core.manager.testimonial')->saveEntity($testimonial, true);

                if ($this->get('sylius.settings.manager')->loadSettings('default')->get('send_notification_to')) {
                    $this->get('corporation_core.mailer_manager')->sendMail([
                        'subject' => $this->get('translator')->trans('new_testimonial.subject', array(), 'email'),
                        'email'   => $this->get('sylius.settings.manager')->loadSettings('default')->get('send_notification_to'),
                        'body'    => $this->get('templating')->render('@CorporationCore/Email/new_testimonial.html.twig', []),
                    ]);
                }

                $response = new Response(json_encode([
                    'status' => 'success',
                ]));
            }
        }

        if (!$response) {
            $page = $request->query->getInt('page', 1);
            $pagination = $this
                ->get('corporation_core.manager.testimonial')
                ->getTestimonialsWithPagination($page, Testimonial::ITEMS_PER_PAGE);

            $response = $this->render('@CorporationCore/Feedback/index.html.twig', [
                'testimonial_form' => $form->createView(),
                'pagination' => $pagination,
            ]);
        }

        return $response;
    }

    /**
     * @Route("/service-request", methods={"GET", "POST"}, name="corporation_service_request")
     */
    public function ajaxServiceRequestAction(Request $request, $formName = null)
    {
        $serviceRequest = new ServiceRequest();
        $form = $this->createForm(ServiceRequestType::class, $serviceRequest);

        $response = null;

        if ($request->isMethod('POST')) {
            $form->submit($request);
            $form->getData();
            if ($form->isValid()) {
                $this->get('corporation_core.manager.service_request')->saveEntity($serviceRequest, true);

                if ($this->get('sylius.settings.manager')->loadSettings('default')->get('send_notification_to')) {
                    $subject  = $this->get('translator')->trans('new_service_request.subject', array(), 'email') . ' ';
                    $subject .= $serviceRequest->getName() ? $serviceRequest->getName() : $serviceRequest->getPhone();

                    $this->get('corporation_core.mailer_manager')->sendMail([
                        'subject' => $subject,
                        'email'   => $this->get('sylius.settings.manager')->loadSettings('default')->get('send_notification_to'),
                        'body'    => $this->get('templating')->render('@CorporationCore/Email/new_service_request.html.twig', [
                            'service_request' => $serviceRequest,
                        ]),
                    ]);
                }

                $response = new Response(json_encode([
                    'status' => 'success',
                ]));
            } else {
                $response = new Response(json_encode([
                    'status' => 'invalid_form',
                ]));
            }
        }

        if (!$response) {
            $response = $this->render('@CorporationCore/Form/service_request.html.twig', [
                'service_request_form' => $form->createView(),
                'form_name' => $formName,
            ]);
        }

        return $response;
    }
}
