<?php
namespace Corporation\CoreBundle\Listener;

use Sonata\MediaBundle\Listener\ORM\MediaEventSubscriber;

class OnClearListener
{
    /**
     * @var MediaEventSubscriber
     */
    protected $mediaBundleSubscriber;
    /**
     * @param MediaEventSubscriber $mediaEventSubscriber
     *
     */
    public function __construct(MediaEventSubscriber $mediaEventSubscriber)
    {
        $this->mediaBundleSubscriber = $mediaEventSubscriber;
    }
    public function onClear()
    {
        $rootCategories = new \ReflectionProperty(MediaEventSubscriber::class, 'rootCategories');
        $rootCategories->setAccessible(true);
        $rootCategories->setValue($this->mediaBundleSubscriber, null);
    }
}