<?php

namespace Corporation\CoreBundle\Entity;

use Sylius\Bundle\SettingsBundle\Model\ParameterInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Parameter
 * @package Corporation\CoreBundle\Entity
 *
 * @ORM\Table(name="settings", uniqueConstraints={@ORM\UniqueConstraint(name="name_idx", columns={"namespace", "name"})}))
 * @ORM\Entity
 */
class Parameter implements ParameterInterface
{
    /**
     * Parameter id.
     *
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * Parameter settings namespace.
     *
     * @var string
     *
     * @ORM\Column(name="namespace", type="string", length=255)
     */
    protected $namespace;

    /**
     * Parameter name.
     *
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    /**
     * Parameter value.
     *
     * @var mixed
     *
     * @ORM\Column(name="value", type="object", nullable=true)
     */
    protected $value;

    /**
     * Get parameter id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * {@inheritdoc}
     */
    public function setNamespace($namespace)
    {
        $this->namespace = $namespace;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }
}
