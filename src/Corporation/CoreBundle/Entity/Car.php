<?php

namespace Corporation\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Sluggable\Util as Sluggable;

/**
 * Car
 *
 * @ORM\Table(name="cars")
 * @ORM\Entity
 */
class Car
{
    const ITEMS_PER_PAGE = 9;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="make", type="string")
     */
    private $make;

    /**
     * @var string
     *
     * @ORM\Column(name="model", type="string")
     */
    private $model;

    /**
     * @var string
     *
     * @ORM\Column(name="engine", type="string")
     */
    private $engine;

    /**
     * @var string
     *
     * @ORM\Column(name="transmission", type="string")
     */
    private $transmission;

    /**
     * @var integer
     *
     * @ORM\Column(name="year", type="integer")
     */
    private $year;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string")
     */
    private $color;

    /**
     * @var array
     *
     * @ORM\Column(name="additional_options", type="json")
     */
    private $additionalOptions = array();

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="price_per_day", type="integer")
     */
    private $pricePerDay;

    /**
     * @var boolean
     *
     * @ORM\Column(name="display_on_main_page", type="boolean")
     */
    private $displayOnMainPage;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="\Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(name="photo", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     *
//     * @Assert\NotBlank(message="photo.required")
     */
    private $mainPhoto;

    /**
     * @ORM\ManyToMany(targetEntity="\Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinTable(
     *     name="car_photos",
     *     joinColumns={
     *      @ORM\JoinColumn(name="car_id", referencedColumnName="id")
     *  },
     *     inverseJoinColumns={
     *      @ORM\JoinColumn(name="photo_id", referencedColumnName="id")
     *  })
     */
    private $photos;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string")
     */
    private $slug;

    /**
     * @ORM\Column(name="seo_title", type="string")
     */
    private $seoTitle;

    /**
     * @ORM\Column(name="seo_description", type="string")
     */
    private $seoDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="seo_keywords", type="string")
     */
    private $seoKeywords;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->photos = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set make
     *
     * @param string $make
     *
     * @return Car
     */
    public function setMake($make)
    {
        $this->make = $make;

        return $this;
    }

    /**
     * Get make
     *
     * @return string
     */
    public function getMake()
    {
        return $this->make;
    }

    /**
     * Set model
     *
     * @param string $model
     *
     * @return Car
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set engine
     *
     * @param string $engine
     *
     * @return Car
     */
    public function setEngine($engine)
    {
        $this->engine = $engine;

        return $this;
    }

    /**
     * Get engine
     *
     * @return string
     */
    public function getEngine()
    {
        return $this->engine;
    }

    /**
     * Set transmission
     *
     * @param string $transmission
     *
     * @return Car
     */
    public function setTransmission($transmission)
    {
        $this->transmission = $transmission;

        return $this;
    }

    /**
     * Get transmission
     *
     * @return string
     */
    public function getTransmission()
    {
        return $this->transmission;
    }

    /**
     * Set year
     *
     * @param integer $year
     *
     * @return Car
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return Car
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Car
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set pricePerDay
     *
     * @param string $pricePerDay
     *
     * @return Car
     */
    public function setPricePerDay($pricePerDay)
    {
        $this->pricePerDay = $pricePerDay;

        return $this;
    }

    /**
     * Get pricePerDay
     *
     * @return string
     */
    public function getPricePerDay()
    {
        return $this->pricePerDay;
    }

    /**
     * Set displayOnMainPage
     *
     * @param boolean $displayOnMainPage
     *
     * @return Car
     */
    public function setDisplayOnMainPage($displayOnMainPage)
    {
        $this->displayOnMainPage = $displayOnMainPage;

        return $this;
    }

    /**
     * Get displayOnMainPage
     *
     * @return boolean
     */
    public function getDisplayOnMainPage()
    {
        return $this->displayOnMainPage;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug =  Sluggable\Urlizer::urlize($slug, '-');
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Car
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set mainPhoto
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $mainPhoto
     *
     * @return Car
     */
    public function setMainPhoto(\Application\Sonata\MediaBundle\Entity\Media $mainPhoto = null)
    {
        $this->mainPhoto = $mainPhoto;

        return $this;
    }

    /**
     * Get mainPhoto
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getMainPhoto()
    {
        return $this->mainPhoto;
    }

    /**
     * Add photo
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $photo
     *
     * @return Car
     */
    public function addPhoto(\Application\Sonata\MediaBundle\Entity\Media $photo)
    {
        $this->photos[] = $photo;

        return $this;
    }

    /**
     * Remove photo
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $photo
     */
    public function removePhoto(\Application\Sonata\MediaBundle\Entity\Media $photo)
    {
        $this->photos->removeElement($photo);
    }

    /**
     * Get photos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * Set seoTitle
     *
     * @param string $seoTitle
     *
     * @return Car
     */
    public function setSeoTitle($seoTitle)
    {
        $this->seoTitle = $seoTitle;

        return $this;
    }

    /**
     * Get seoTitle
     *
     * @return string
     */
    public function getSeoTitle()
    {
        return $this->seoTitle;
    }

    /**
     * Set seoDescription
     *
     * @param string $seoDescription
     *
     * @return Car
     */
    public function setSeoDescription($seoDescription)
    {
        $this->seoDescription = $seoDescription;

        return $this;
    }

    /**
     * Get seoDescription
     *
     * @return string
     */
    public function getSeoDescription()
    {
        return $this->seoDescription;
    }

    /**
     * Set seoKeywords
     *
     * @param string $seoKeywords
     *
     * @return Car
     */
    public function setSeoKeywords($seoKeywords)
    {
        $this->seoKeywords = $seoKeywords;

        return $this;
    }

    /**
     * Get seoKeywords
     *
     * @return string
     */
    public function getSeoKeywords()
    {
        return $this->seoKeywords;
    }

    /**
     * Set additionalOptions
     *
     * @param array $additionalOptions
     *
     * @return Car
     */
    public function setAdditionalOptions($additionalOptions)
    {
        $this->additionalOptions = $additionalOptions;

        return $this;
    }

    /**
     * Get additionalOptions
     *
     * @return array
     */
    public function getAdditionalOptions()
    {
        return $this->additionalOptions;
    }
}
