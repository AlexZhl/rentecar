<?php

namespace Corporation\CoreBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\ConstraintValidatorInterface;

class PasswordValidator extends ConstraintValidator implements ConstraintValidatorInterface
{
    public $groups = ['Password'];
    public $constraint;
    public $resetPageEmail = null;

    public function validate($value, Constraint $constraint)
    {
        $this->constraint = $constraint;
        $email = $this->constraint->getEmail();
        if (!$email) $email = $this->resetPageEmail;
        $emailPart = $this->splitEmail($email);

        if (strpos($value, $emailPart) !== false) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('%string%', $value)
                ->addViolation();
        }
    }

    private function splitEmail($email) {
        $parts = explode('@', $email);
        return $parts[0];
    }

    public function setEmail($resetPageEmail)
    {
        $this->resetPageEmail = $resetPageEmail;
    }
}
