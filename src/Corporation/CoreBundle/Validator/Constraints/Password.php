<?php

namespace Corporation\CoreBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\ConstraintValidatorInterface;

class Password extends Constraint
{
    public $message = "password.too.simple";
    public $email = null;

    public function __construct($options)
    {
        parent::__construct($options);
        if (isset($options['email'])) $this->email = $options['email'];
    }

    public function validatedBy()
    {
        return 'corporation_password_validation';
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

}
