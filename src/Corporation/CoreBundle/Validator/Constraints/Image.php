<?php

namespace Corporation\CoreBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class Image extends Constraint
{
    public $message = 'Please upload a JPEG of 4MB or less';
    public $maxSize;
    public $ext;
}
