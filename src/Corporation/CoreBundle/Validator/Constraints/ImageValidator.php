<?php

namespace Corporation\CoreBundle\Validator\Constraints;

use Application\Sonata\MediaBundle\Entity\Media;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ImageValidator extends ConstraintValidator
{
    /**
     * @param null|Media $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if ($value instanceof Media) {
            $size = $value->getSize();
            $ext = mb_strtolower($value->getExtension());

            if ($size > $constraint->maxSize || !in_array($ext, $constraint->ext)) {
                $this->context->buildViolation($constraint->message)
                    ->addViolation();
            }
        }
    }
}
