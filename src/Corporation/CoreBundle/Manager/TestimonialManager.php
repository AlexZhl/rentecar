<?php
namespace Corporation\CoreBundle\Manager;

use Knp\Component\Pager\Paginator;

class TestimonialManager extends BaseManager
{
    private $paginator;

    public function setPaginator(Paginator $paginator)
    {
        $this->paginator = $paginator;
    }

    public function getPaginator()
    {
        return $this->paginator;
    }

    public function getTestimonialsWithPagination($page, $itemsPerPage)
    {
        $qb = $this->getRepository()->createQueryBuilder('testimonial');
        $query = $qb
            ->andWhere('testimonial.approved = :yes')
            ->setParameters(['yes' => 1])
            ->getQuery();

        return $this->paginator->paginate($query, $page, $itemsPerPage);
    }
}
