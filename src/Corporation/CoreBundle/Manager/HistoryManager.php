<?php

namespace Corporation\CoreBundle\Manager;

use Application\Sonata\ClassificationBundle\Entity\Tag;
use Doctrine\ORM\Query\Expr;
use Application\Sonata\NewsBundle\Entity\HistoryPost;

/**
 * Class CategoryManager.
 */
class HistoryManager extends BaseManager
{
    public function getHistory()
    {
        $qb = $this->getRepository()->createQueryBuilder('history');
        $result = $qb
            ->andWhere('history.enabled = :enabled')
            ->setParameters(['enabled' => HistoryPost::ENABLED])
            ->getQuery()
            ->getResult();

        return $result;
    }

}
