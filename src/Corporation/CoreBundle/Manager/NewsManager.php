<?php

namespace Corporation\CoreBundle\Manager;

use Application\Sonata\ClassificationBundle\Entity\Tag;
use Doctrine\ORM\Query\Expr;
use Application\Sonata\NewsBundle\Entity\NewsPost;

/**
 * Class CategoryManager.
 */
class NewsManager extends BaseManager
{
    public function getNews()
    {
        $qb = $this->getRepository()->createQueryBuilder('news');
        $result = $qb
            ->andWhere('news.enabled = :enabled')
            ->setParameters(['enabled' => NewsPost::ENABLED])
            ->getQuery()
            ->getResult();

        return $result;
    }

}
