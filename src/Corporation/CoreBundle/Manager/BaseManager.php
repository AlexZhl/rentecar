<?php

namespace Corporation\CoreBundle\Manager;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class BaseManager.
 */
abstract class BaseManager implements ManagerInterface
{
    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var string
     */
    protected $class;

    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    protected $repository;

    /**
     * @var TokenStorageInterface
     */
    public $tokenStorage;

    /**
     * @var \TDM\DoctrineEncryptBundle\Encryptor\EncryptorInterface
     */
    protected $encryptor;

    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function setEm($em)
    {
        $this->em = $em;
    }

    /**
     * @param RequestStack $requestStack
     */
    public function setRequestStack(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @param string $class
     */
    public function setClass($class)
    {
        $this->repository = $this->em->getRepository($class);
        $metadata = $this->em->getClassMetadata($class);
        $this->class = $metadata->getName();
    }

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function setTokenStorage(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @return EntityManager
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @return mixed
     */
    public function newEntity()
    {
        $class = $this->getClass();

        return new $class();
    }

    /**
     * @param $entity
     * @param bool $andFlush
     */
    public function saveEntity($entity, $andFlush = true)
    {
        $this->em->persist($entity);
        if ($andFlush) {
            $this->em->flush();
        }
    }

    public function flush()
    {
        $this->em->flush();
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function find($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @param array $array
     *
     * @return object
     */
    public function findOneBy(array $array)
    {
        return $this->repository->findOneBy($array);
    }

    /**
     * @param array $array
     *
     * @return object
     */
    public function findOneByEncrypted(array $array)
    {
        foreach ($array as $k => $v) {
            $array[$k] = $this->encryptString($v);
        }

        return $this->repository->findOneBy($array);
    }

    /**
     * @return array
     */
    public function findAll()
    {
        return $this->repository->findAll();
    }

    /**
     * Finds entities by a set of criteria.
     *
     * @param array      $criteria
     * @param array|null $orderBy
     * @param int|null   $limit
     * @param int|null   $offset
     *
     * @return array The objects.
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * Finds entities by a set of criteria (encrypted fields).
     *
     * @param array      $criteria
     * @param array|null $orderBy
     * @param int|null   $limit
     * @param int|null   $offset
     *
     * @return array The objects.
     */
    public function findByEncrypted(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        foreach ($criteria as $k => $v) {
            $criteria[$k] = $this->encryptString($v);
        }

        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param $entity
     */
    public function deleteEntity($entity, $andFlush = true)
    {
        $this->em->remove($entity);
        if ($andFlush) {
            $this->em->flush();
        }
    }

    /**
     * @param array $entities
     * @param int   $batchSize
     */
    public function deleteEntities(array $entities, $batchSize = 10)
    {
        if (!empty($entities)) {
            $i = 0;
            $j = 0;
            $em = $this->getEm();
            foreach ($entities as $entity) {
                $em->remove($entity);
                ++$i;
                ++$j;
                if (0 == ($i % $batchSize)) {
                    $em->flush();
                    $em->clear($this->getClass());
                    $j = 0;
                }
            }
            if (0 < $j) {
                $em->flush();
            }
        }
    }

    /**
     * @param array $criteria
     *
     * @return object
     */
    public function findEntityBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * @param $entity
     */
    public function reloadEntity($entity)
    {
        $this->em->refresh($entity);
    }

    /**
     * Sets encryptor.
     *
     * @param \TDM\DoctrineEncryptBundle\Encryptor\EncryptorInterface $encryptor
     */
    public function setEncryptor($encryptor)
    {
        $this->encryptor = $encryptor;
    }

    /**
     * Returns encryptor.
     *
     * @return \TDM\DoctrineEncryptBundle\Encryptor\EncryptorInterface
     */
    public function getEncryptor()
    {
        return $this->encryptor;
    }

    /**
     * Returns encrypted string.
     *
     * @param string $string
     *
     * @return string
     */
    protected function encryptString($string)
    {
        return $this->getEncryptor()->encrypt($string, true);
    }
}
