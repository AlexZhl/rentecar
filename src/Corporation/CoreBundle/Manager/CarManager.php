<?php
namespace Corporation\CoreBundle\Manager;

use Knp\Component\Pager\Paginator;

class CarManager extends BaseManager
{
    private $paginator;

    public function setPaginator(Paginator $paginator)
    {
        $this->paginator = $paginator;
    }

    public function getPaginator()
    {
        return $this->paginator;
    }

    public function getCar($slug)
    {
        $qb = $this->getRepository()->createQueryBuilder('car');
        $query = $qb
            ->andWhere('car.slug = :slug')
            ->setParameters(['slug' => $slug])
            ->getQuery();

        $result = $query->getResult();

        // TODO: Make 'slug' unique
        return array_shift($result);
    }

    public function getCarsWithPagination($page, $itemsPerPage, $isMainPage = false)
    {
        $qb = $this->getRepository()->createQueryBuilder('car');

        if ($isMainPage) {
            $query = $qb
                ->andWhere('car.displayOnMainPage = :isMainPage')
                ->setParameters(['isMainPage' => $isMainPage]);
        }

        $query = $qb->getQuery();

        return $this->paginator->paginate($query, $page, $itemsPerPage);
    }
}
