<?php

namespace Corporation\CoreBundle\Manager;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bridge\Monolog\Logger;

class MailManager
{
    /**
     * @var ContainerInterface
     */
    protected $container;
    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * @param ContainerInterface $container
     * @param Logger             $logger
     * @param \Swift_Mailer      $mailer
     */
    public function __construct(ContainerInterface $container, \Swift_Mailer $mailer)
    {
        $this->container = $container;
        $this->mailer = $mailer;
    }

    public function sendMail($params)
    {
        if (!isset($params['content_type'])) {
            $params['content_type'] = 'text/html';
        }

        if (!isset($params['from'])) {
            $params['from'] = array($this->container->getParameter('noreply_email') => $this->container->getParameter('noreply_name'));
        }

        $message = \Swift_Message::newInstance()
            ->setSubject($params['subject'])
            ->setFrom($params['from'])
            ->setTo($params['email'])
            ->setBody($params['body'])
            ->setContentType($params['content_type']);

//        if (!empty($params['attachments'])) {
//            $fs = new Filesystem();
//            foreach ($params['attachments'] as $attachment) {
//                if ( ! $fs->exists($attachment)) {
//                    continue;
//                }
//
//                try {
//                    $message->attach(\Swift_Attachment::fromPath($attachment));
//                } catch (\Exception $e) {
//                    $this->logger->critical('[mail.consumer] Attachments error.', ['error' => $e->getMessage()]);
//                    return;
//                }
//            }
//        }

        $this->mailer->send($message);
        $this->mailer->getTransport()->stop();
    }
}
