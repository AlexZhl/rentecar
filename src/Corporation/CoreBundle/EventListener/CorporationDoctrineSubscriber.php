<?php

namespace Corporation\CoreBundle\EventListener;

use Application\Sonata\MediaBundle\Entity\Media;
use Application\Sonata\UserBundle\Entity\Admin;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\Translation\TranslatorInterface;

class CorporationDoctrineSubscriber implements EventSubscriber, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getSubscribedEvents()
    {
        return array(
            'postUpdate',
        );
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();
        $entity = $args->getEntity();
        $changeSet = $uow->getEntityChangeSet($entity);
        $mailerManager = $this->container->get('corporation_core.mailer_manager');

        if ($entity instanceof Admin && isset($changeSet['twoStepVerificationCode'])) {
            $params = [];
            $params['subject'] = $this->container->get('translator')->trans('admin_secret_code_changed.subject', array(), 'email');
            $params['email'] = $entity->getEmail();
            $params['body'] = $this->container->get('templating')->render(
                '@CorporationCore/Email/admin_secret_code_changed.html.twig',
                [
                    'user' => $entity,
                ]
            );

            $mailerManager->sendMail($params);
        }
    }

}