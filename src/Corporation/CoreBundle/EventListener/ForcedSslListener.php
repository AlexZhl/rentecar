<?php

namespace Corporation\CoreBundle\EventListener;

use Nelmio\SecurityBundle\EventListener\ForcedSslListener as BaseForcedSslListener;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class ForcedSslListener extends BaseForcedSslListener
{
    public function onKernelRequest(GetResponseEvent $e)
    {
        return;
    }
}
