<?php

namespace Corporation\CoreBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Csrf\TokenStorage\NativeSessionTokenStorage;

class CsrfRemoveSubscriber implements EventSubscriberInterface
{
    /**
     * The name of the CSRF field.
     *
     * @var string
     */
    private $fieldName;

    /**
     * The generator for CSRF tokens.
     *
     * @var CsrfTokenManagerInterface
     */
    private $tokenManager;

    /**
     * A text mentioning the tokenId of the CSRF token.
     *
     * Validation of the token will only succeed if it was generated in the
     * same session and with the same tokenId.
     *
     * @var string
     */
    private $tokenId;

    /**
     * @var SessionInterface
     */
    private $session;

    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::POST_SUBMIT => 'postSubmit',
        );
    }

    public function __construct($fieldName, $tokenManager, $tokenId, SessionInterface $session = null)
    {
        $this->fieldName = $fieldName;
        $this->tokenManager = $tokenManager;
        $this->tokenId = $tokenId;
        $this->session = $session;
    }

    public function postSubmit(FormEvent $event)
    {
        if (!$this->session || !$this->session->isStarted()) {
            return;
        }

        $form = $event->getForm();
        if ($form->isRoot() && $form->getConfig()->getOption('compound')) {
            if (!$form->isValid()) {
                foreach ($form->getErrors() as $error) {
                    // CSRF error occurred, so clearing all
                    // do not have another way to detect CSRF error
                    if (false !== stripos($error->getMessage(), 'csrf')) {
                        foreach ($this->session->all() as $k => $item) {
                            if (0 === strpos($k, NativeSessionTokenStorage::SESSION_NAMESPACE)) {
                                $token = str_replace(NativeSessionTokenStorage::SESSION_NAMESPACE.'/', '', $k);
                                $this->tokenManager->removeToken($token);
                            }
                        }

                        break;
                    }
                }
            }
        }
    }
}
