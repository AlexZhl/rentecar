<?php

namespace Corporation\CoreBundle\Form\Handler;

use Corporation\CoreBundle\Form\Type\NewsLetterType;
use Corporation\CoreBundle\Manager\NewsLetterManager;
use Corporation\CoreBundle\Entity\NewsLetter;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class NewsLetterHandler
{

    private $_container;

    public function __construct(Container $serviceContainer)
    {
        $this->_container = $serviceContainer;
    }

    public function getNewsLetterForm()
    {
        $request = $this->_container->get('request');


        $newsLetterType = new NewsLetterType();
        $newsLetterForm = $this->_container->get('form.factory')->create($newsLetterType, null, ['method' => 'POST']);

        try {
            $newsLetterForm->handleRequest($request);
        } catch (UnexpectedTypeException $e) {
            return false;
        }

        return $newsLetterForm;
    }

    public function saveNewsLetterForm($newsLetterForm)
    {
        if ($newsLetterForm->isValid()) {
            $newsLetter = $newsLetterForm->getData();
            /** @var NewsLetter $newsLetter */
            $newsLetter->setStatus(NewsLetter::STATUS_PENDING);
            $newsLetter->setConfirmationToken($this->_container->get('fos_user.util.token_generator')->generateToken());

            $newsLetter->setCreatedAt(new \DateTime());

            /** @var NewsLetterManager $newsLetterManager */
            $newsLetterManager = $this->_container->get('corporation.newsletter.manager');

            $newsLetterManager->saveEntity($newsLetter, true);

            return true;
        }

        return false;
    }

    public function isSubmitted(Request $request)
    {
        if ($request->request->has((new NewsLetterType())->getName())) {
            return true;
        } else {
            return false;
        }
    }

}
