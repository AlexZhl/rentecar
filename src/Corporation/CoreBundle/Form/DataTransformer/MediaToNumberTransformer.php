<?php

namespace Corporation\CoreBundle\Form\DataTransformer;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class MediaToNumberTransformer implements DataTransformerInterface
{
    //private $manager;

    public function __construct(/* ObjectManager $manager */)
    {
        //$this->manager = $manager;
    }

    /**
     * Transforms an object (mediaItem) to a string (number).
     *
     * @param mediaItem|null $mediaItem
     *
     * @return string
     */
    public function transform($mediaItem)
    {
        echo 'b';
        exit;
        var_dump($mediaItem);
        exit;

        return $mediaItem;

        echo $mediaItem->getId();
        exit;

        return;

        if (!$mediaItem) {
            return '';
        }

        return $mediaItem->getId();
    }

    /**
     * Transforms a string (number) to an object (mediaItem).
     *
     * @param string $mediaItemNumber
     *
     * @return mediaItem|null
     *
     * @throws TransformationFailedException if object (mediaItem) is not found.
     */
    public function reverseTransform($mediaItemNumber)
    {
        echo 'v';
        exit;

        return;
        // no mediaItem number? It's optional, so that's ok
        if (!$mediaItemNumber) {
            return;
        }

        $mediaItem = $this->manager
            ->getRepository('AppBundle:mediaItem')
            // query for the mediaItem with this id
            ->find($mediaItemNumber)
        ;

        if (null === $mediaItem) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'An mediaItem with number "%s" does not exist!',
                $mediaItemNumber
            ));
        }

        return $mediaItem;
    }
}
