<?php

namespace Corporation\CoreBundle\Form\Extension;

use Corporation\CoreBundle\EventListener\CsrfRemoveSubscriber;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\Extension\Csrf\CsrfProvider\CsrfProviderAdapter;
use Symfony\Component\Form\Extension\Csrf\CsrfProvider\CsrfProviderInterface;
use Symfony\Component\Form\Extension\Csrf\CsrfProvider\CsrfTokenManagerAdapter;
use Symfony\Component\Form\Extension\Csrf\EventListener\CsrfValidationListener;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class FormTypeCsrfExtension extends AbstractTypeExtension
{
    const CSRF_NAME = 'token';

    /**
     * @var CsrfTokenManagerInterface
     */
    private $defaultTokenManager;

    /**
     * @var bool
     */
    private $defaultEnabled;

    /**
     * @var string
     */
    private $defaultFieldName;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var null|string
     */
    private $translationDomain;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @param SessionInterface $session
     */
    public function setSession(SessionInterface $session = null)
    {
        $this->session = $session;
    }

    public function __construct($defaultTokenManager, $defaultEnabled = true, $defaultFieldName = '_token', TranslatorInterface $translator = null, $translationDomain = null)
    {
        if ($defaultTokenManager instanceof CsrfProviderInterface) {
            $defaultTokenManager = new CsrfProviderAdapter($defaultTokenManager);
        } elseif (!$defaultTokenManager instanceof CsrfTokenManagerInterface) {
            throw new UnexpectedTypeException($defaultTokenManager, 'CsrfProviderInterface or CsrfTokenManagerInterface');
        }

        $this->defaultTokenManager = $defaultTokenManager;
        $this->defaultEnabled = $defaultEnabled;
        $this->defaultFieldName = $defaultFieldName;
        $this->translator = $translator;
        $this->translationDomain = $translationDomain;
    }

    /**
     * Adds a CSRF field to the form when the CSRF protection is enabled.
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (!$options['csrf_protection']) {
            return;
        }

        $builder
            ->addEventSubscriber(new CsrfValidationListener(
                $options['csrf_field_name'],
                $options['csrf_token_manager'],
                self::CSRF_NAME,
                $options['csrf_message'],
                $this->translator,
                $this->translationDomain
            ))
        ;
        $builder
            ->addEventSubscriber(new CsrfRemoveSubscriber(
                $options['csrf_field_name'],
                $options['csrf_token_manager'],
                self::CSRF_NAME,
                $this->session
            ))
        ;
    }

    /**
     * Adds a CSRF field to the root form view.
     *
     * @param FormView      $view    The form view
     * @param FormInterface $form    The form
     * @param array         $options The options
     */
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        if ($options['csrf_protection'] && !$view->parent && $options['compound']) {
            $factory = $form->getConfig()->getFormFactory();
            $tokenId = self::CSRF_NAME;
            $data = (string) $options['csrf_token_manager']->getToken($tokenId);

            $csrfForm = $factory->createNamed($options['csrf_field_name'], 'hidden', $data, array(
                'mapped' => false,
            ));

            $view->children[$options['csrf_field_name']] = $csrfForm->createView($view);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        // BC clause for the "intention" option
        $csrfTokenId = function (Options $options) {
            return $options['intention'];
        };

        // BC clause for the "csrf_provider" option
        $csrfTokenManager = function (Options $options) {
            if ($options['csrf_provider'] instanceof CsrfTokenManagerInterface) {
                return $options['csrf_provider'];
            }

            return $options['csrf_provider'] instanceof CsrfTokenManagerAdapter
                ? $options['csrf_provider']->getTokenManager(false)
                : new CsrfProviderAdapter($options['csrf_provider']);
        };

        $resolver->setDefaults(array(
            'csrf_protection' => $this->defaultEnabled,
            'csrf_field_name' => $this->defaultFieldName,
            'csrf_message' => 'The CSRF token is invalid. Please try to resubmit the form.',
            'csrf_token_manager' => $csrfTokenManager,
            'csrf_token_id' => $csrfTokenId,
            'csrf_provider' => $this->defaultTokenManager,
            'intention' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getExtendedType()
    {
        return 'form';
    }
}
