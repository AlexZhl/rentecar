<?php

namespace Corporation\CoreBundle\Block;

use Corporation\MenuBundle\Entity\Menu;
use Sonata\Cache\Invalidation\Recorder;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Block\BaseBlockService;

/**
 * Class FooterMenuBlockService.
 */
class HeaderMenuBlockService extends BaseBlockService
{
    /**
     * @var string
     */
    protected $env;

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    protected $request;

    /**
     * @var \Sonata\Cache\Invalidation\Recorder
     */
    protected $recorder;

    /**
     * @var \Knp\Menu\Provider\MenuProviderInterface
     */
    protected $menuProvider;

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        try {
            $menu = $this->menuProvider->get('main');
        } catch (\InvalidArgumentException $e) {
            return new Response();
        }

        $this->recorder->add($menu);
        $this->recorder->add(array('Application\Sonata\PageBundle\Entity\Page', 'default'));
        $this->recorder->add(array('Application\Sonata\PageBundle\Entity\Snapshot', 'default'));

        return $this->renderResponse($blockContext->getTemplate(), array(
            'menu' => $menu,
            'context' => $blockContext,
            'settings' => $blockContext->getSettings(),
            'block' => $blockContext->getBlock(),
        ), $response);
    }

    /**
    * {@inheritdoc}
    */
    public function getName()
    {
        return 'Header Menu';
    }

    /**
     * {@inheritdoc}
     */
    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'template' => '@CorporationCore/Block/block_header_menu.html.twig',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getCacheKeys(BlockInterface $block)
    {
        $path = $this->getRequest()->getPathInfo();

        return array(
            'name'   => $this->getName(),
            'uri' => $path,
            'env' => $this->env,
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     *
     * @param \Sonata\Cache\Invalidation\Recorder $recorder
     */
    public function setRecorder(Recorder $recorder)
    {
        $this->recorder = $recorder;
    }

    public function setMenuProvider($menuProvider)
    {
        $this->menuProvider = $menuProvider;
    }

    /**
     * @param string $env
     */
    public function setEnv($env)
    {
        $this->env = $env;
    }
}
