<?php


namespace Corporation\MenuBundle\Entity\Repository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Gedmo\Sortable\SortableListener;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Corporation\MenuBundle\Exception\InvalidArgumentException;
use Knp\Menu\Factory\CoreExtension;

class MenuRepository extends NestedTreeRepository implements MenuItemRepositoryInterface
{
    public function getMaxPositionByParentId($parent = null)
    {

        $qb = $this->createQueryBuilder('e');
        $qb->select('MAX(e.position) as max_position');

        if ($parent != null) {
            $qb->where('e.parent = :parent');
            $qb->setParameter('parent', $parent);
        } else {
            $qb->where('e.parent IS NULL');
        }

        $res = $qb->getQuery()->getResult();

        return $res[0]['max_position'];
    }

    public function getParentChildNumber()
    {
        $qb = $this->createQueryBuilder('m')
            ->select('IDENTITY(m.parent) as parent')
            ->addSelect('COUNT(m) as size')
            ->groupBy('m.parent')
            ->addOrderBy('m.root', 'ASC')
            ->addOrderBy('m.lft', 'ASC')
        ;

        return $qb->getQuery()->getResult();
    }


    public function setRoutingExtension($routingExtension)
    {
        $this->routingExtension = $routingExtension;
    }

    /** @var boolean */
    protected $cacheLoaded;

    /** @var array */
    protected $nameArray;

    /**
     * Constructor.
     *
     * @param EntityManager $em    The EntityManager to use.
     * @param ClassMetadata $class The class descriptor.
     */
    public function __construct($em, ClassMetadata $class)
    {
        $this->cacheLoaded = false;
        $this->nameArray = array();

        parent::__construct($em, $class);
    }

    public function getMenuItemByName($name)
    {
        $this->populateCache();

        if (!array_key_exists($name, $this->nameArray)) {
            return null;
        }

        return $this->nameArray[$name];
    }

    /**
     * Will query all the menu items at and sort them for the cache.
     */
    protected function populateCache()
    {
        if (!$this->cacheLoaded) {
            // Query two levels deep.
            $allMenuItems = $this->createQueryBuilder('m')
                ->addSelect('children')
                ->leftJoin('m.children', 'children', 'WITH', 'children.enabled = true')
                //->addOrderBy('m.parent', 'ASC')
                //->addOrderBy('m.position', 'ASC')
                //->where('m.enabled = true')
                ->getQuery()
                ->useResultCache(true)
                ->getResult();


            $c = new CoreExtension();

            foreach ($allMenuItems as $menuItem) {
                $type = $menuItem->getType();

                if ('route' == $type) {
                    $route = $menuItem->getLink();
                    $parameters = $menuItem->getRouteParameters();
                    $childRoutes = $menuItem->getChildRoutes();
                    $options = $this->routingExtension->buildOptions(array(
                        'route' => $route,
                        'routeParameters' => $parameters,
                        'attributes' => $menuItem->getAttributes(),
                        'linkAttributes' => $menuItem->getLinkAttributes(),
                        'labelAttributes' => $menuItem->getLabelAttributes(),
                        'extras' => array(
                            'routes' => $childRoutes
                        )
                    ));
                    $options = $c->buildOptions($options);

                    $c->buildItem($menuItem, $options);
                } else {
                    $menuItem->setUri($menuItem->getLink());
                }

                $this->nameArray[$menuItem->getName()] = $menuItem;
            }

            $this->cacheLoaded = true;
        }
    }
}
