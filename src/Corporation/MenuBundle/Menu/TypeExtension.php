<?php

namespace Corporation\MenuBundle\Menu;

use Knp\Menu\Factory\ExtensionInterface;
use Knp\Menu\ItemInterface;

/**
 * core factory extension with the main logic
 */
class TypeExtension implements ExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function buildOptions(array $options)
    {
        return array_merge(
            array(
                'type' => null,
                'link' => null,
                'routeParameters' => array(),
                'childRoutes' => array(),
            ),
            $options
        );
    }

    /**
     * {@inheritdoc}
     */
    public function buildItem(ItemInterface $item, array $options)
    {
        $item
            ->setType($options['type'])
            ->setLink($options['link'])
            ->setRouteParameters($options['routeParameters'])
            ->setChildRoutes($options['childRoutes'])
        ;
    }
}
