<?php
namespace Corporation\MenuBundle\Doctrine\DBAL\Types;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Corporation\CoreBundle\Doctrine\DBAL\Types\AbstractEnumType;

class LinkType extends AbstractEnumType
{
    protected $name = 'enumlinktype';
    protected $values = array(null, 'uri', 'route');
}