<?php

namespace Corporation\MenuBundle\Admin;

use Corporation\MenuBundle\Entity\Repository\MenuRepository;
use Corporation\MenuBundle\Util\PositionHandler;
use Sonata\AdminBundle\Admin\Admin;
use Corporation\MenuBundle\Entity\Menu;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\CoreBundle\Validator\ErrorElement;
use Symfony\Component\Validator\Constraints as Assert;
use Burgov\Bundle\KeyValueFormBundle\Form\Type\KeyValueType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Form\Type\ChoiceFieldMaskType;
use \Sonata\AdminBundle\Form\Type\CollectionType;

class MenuAdmin extends Admin
{
    protected
        $managerRegistry,
        $formAttribute = array(),
        $positionHandler
    ;

    public function __construct($code, $class, $baseControllerName)
    {
        $this->positionHandler       = new PositionHandler();

        parent::__construct($code, $class, $baseControllerName);
    }

    /**
     * @return array
     */
    public function getPersistentParameters()
    {
        return array(
            'provider'  => $this->getRequest()->get('provider'),
        );
    }

    /**
     * @param   string $context
     *
     * @return \Sonata\AdminBundle\Datagrid\ProxyQueryInterface
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        $query->addOrderBy($query->getRootAlias() .'.root', 'ASC');
        $query->addOrderBy($query->getRootAlias() .'.lft', 'ASC');

        return $query;
    }

    /**
     * @param ErrorElement $errorElement
     * @param mixed $object
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
            ->with('parent')
            ->addConstraint(
                new Assert\NotEqualTo(
                    array('value' => $object)
                )
            )
            ->end();
    }

    /**
     * @param  $positionHandler
     * @return $this
     */
    public function setPositionHandler($positionHandler)
    {
        $this->positionHandler = $positionHandler;

        return $this;
    }

    /**
     * @return PositionHandler
     */
    public function getPositionHandler()
    {
        return $this->positionHandler;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter().'/move/{position}');
        $collection->add('activate', $this->getRouterIdParameter().'/activate');
    }

    /**
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name')
            ->add('parent')
            ->add('active')
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        if ($this->getSubject() && $this->getSubject()->getId()) {
            $this->initializeEditForm();
        } else {
            $this->initializeCreateForm();
        }

        $formMapper
            ->add('name', 'text')
            ->add('parent', 'entity', array(
                'class'    => 'Corporation\MenuBundle\Entity\Menu',
                'property' => 'indentedTitle',
                'query_builder' => function(MenuRepository $mr){
                    return $mr->createQueryBuilder('i')
                        ->orderBy('i.lft', 'ASC');
                }
            ))
            ->add('enabled', null, array('required' => false))
            ->add('type', ChoiceFieldMaskType::class, array(
                'choices' => array(
                    '' => '',
                    'uri' => 'uri',
                    'route' => 'route',
                ),
                'map' => array(
                    'uri' => array('link'),
                    'route' => array('link', 'routeParameters'),
                ),
                'required' => false,
            ))
            ->add('link', null, array('required' => false))
            ->add('routeParameters', KeyValueType::class, array(
                'value_type' => TextType::class,
                'value_options' => array(
                    'required' => false
                )
            ))
            ->add('childRoutes', CollectionType::class, array(
                'required' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'entry_type' => TextType::class,
                'entry_options' => array(
                    'required' => false
                )
            ))
            ->add('attributes', KeyValueType::class, array(
                'value_type' => TextType::class,
                'value_options' => array(
                    'required' => false
                )
            ))
            ->add('linkAttributes', KeyValueType::class, array(
                'value_type' => TextType::class,
                'value_options' => array(
                    'required' => false
                )
            ))
            ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $this->positionHandler->setLastPositions($this->getLastPositionsFromDb());

        $listMapper
            ->addIdentifier('id')
            ->add('name', 'string', array('template' => 'CorporationMenuBundle:Admin:base_list_field.html.twig'))
            ->add('parent' , 'list')
            ->add('enabled')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit'      => array(),
                    'delete'    => array(),
                    'move'      => array('template' => 'CorporationMenuBundle:Admin:list__action_sort.html.twig')
                    )
                )
            )
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('parent')
        ;
    }

    protected function initializeEditForm()
    {
        $this->formAttribute = array();
    }

    protected function initializeCreateForm()
    {
        $this->formAttribute = array('checked' => 'checked');
    }

    /**
     * @return array
     */
    protected function getLastPositionsFromDb()
    {
        $repo = $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository($this->getClass());

        $count = array();

        foreach ($repo->getParentChildNumber() as $data) {

            if ($data['parent'] == null ) {
                $data['parent'] = 0;
            }

            $count[$data['parent']] = $data['size'];
        }

        return $count;
    }

    public function getNewInstance()
    {
        $name = '';
        return new Menu($name, $this->menuFactory);
    }
}
