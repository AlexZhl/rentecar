<?php

$time = microtime(true);

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Debug\Debug;
use Symfony\Component\HttpFoundation\IpUtils;

umask(0000);

//$loader = require_once __DIR__.'/../app/autoload.php';
$loader = require_once __DIR__.'/../app/bootstrap.php.cache';
Request::setTrustedHeaderName(Request::HEADER_CLIENT_PROTO, 'CloudFront-Forwarded-Proto');
Request::setTrustedHeaderName(Request::HEADER_CLIENT_PORT, 'CloudFront-Forwarded-Port');

// If you don't want to setup permissions the proper way, just uncomment the following PHP line
// read http://symfony.com/doc/current/book/installation.html#configuration-and-setup for more information
//umask(0000);

// This check prevents access to debug front controllers that are deployed by accident to production servers.
// Feel free to remove this, extend it, or make something more sophisticated.

//Retrieve IP
$ip = '';
$httpForwardedFor = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : '';
$ips = explode(',', $httpForwardedFor);
if (!empty($ips[0])) {
    $ip = $ips[0];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}

if (
    !IpUtils::checkIp($ip, array('127.0.0.1', 'fe80::1', '::1'))
    && !IpUtils::checkIp($ip, ['93.170.143.0/24', '93.171.6.0/24', '10.0.101.101/20'])
)
{
    header('Cache-Control: no-cache, no-store, must-revalidate');
    header('Pragma: no-cache');
    header('Expires: 0');

    header('HTTP/1.0 403 Forbidden');
    exit('You are not allowed to access this file. Check '.basename(__FILE__).' for more information.');
}

Debug::enable();

require_once __DIR__.'/../app/AppKernel.php';

$kernel = new AppKernel('dev', true);
$kernel->loadClassCache();


$request = Request::createFromGlobals();
Request::setTrustedProxies(array('127.0.0.1', $request->server->get('REMOTE_ADDR')));
$response = $kernel->handle($request);


$response->headers->add([
    'X-Time' => microtime(true) - $time,
]);

$response->send();
$kernel->terminate($request, $response);
//echo '<!-- ' . (microtime(true) - $time) . '-->';