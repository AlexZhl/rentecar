<?php

$time = microtime(true);

use Symfony\Component\ClassLoader\ApcClassLoader;
use Symfony\Component\HttpFoundation\Request;

umask(0000);

$loader = require_once __DIR__.'/../app/bootstrap.php.cache';
Request::setTrustedHeaderName(Request::HEADER_CLIENT_PROTO, 'CloudFront-Forwarded-Proto');
Request::setTrustedHeaderName(Request::HEADER_CLIENT_PORT, 'CloudFront-Forwarded-Port');

// Enable APC for autoloading to improve performance.
// You should change the ApcClassLoader first argument to a unique prefix
// in order to prevent cache key conflicts with other applications
// also using APC.
/*
$apcLoader = new ApcClassLoader(sha1(__FILE__), $loader);
$loader->unregister();
$apcLoader->register(true);
*/

require_once __DIR__.'/../app/AppKernel.php';

$kernel = new AppKernel('prod', false);
$kernel->loadClassCache();

// When using the HttpCache, you need to call the method in your front controller instead of relying on the configuration parameter
//Request::enableHttpMethodParameterOverride();
$request = Request::createFromGlobals();
Request::setTrustedProxies(array('127.0.0.1', $request->server->get('REMOTE_ADDR')));
$response = $kernel->handle($request);

$currentXTime = $response->headers->get('X-Time', null);
$xTime = (microtime(true) - $time) . ($currentXTime ? '/' . $currentXTime : '');

$response->headers->add([
    'X-Time' => $xTime,
]);

$response->send();
$kernel->terminate($request, $response);
//echo '<!-- ' . (microtime(true) - $time) . '-->';