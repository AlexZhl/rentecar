<?php


use Symfony\Component\HttpFoundation\IpUtils;

require_once __DIR__.'/../app/bootstrap.php.cache';

header('Cache-Control: no-cache, no-store, must-revalidate');
header('Pragma: no-cache');
header('Expires: 0');

//Retrieve IP
$ip = '';
$httpForwardedFor = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : '';
$ips = explode(',', $httpForwardedFor);
if (!empty($ips[0])) {
    $ip = $ips[0];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}

if (
    !IpUtils::checkIp($ip, array('127.0.0.1', 'fe80::1', '::1'))
    && !IpUtils::checkIp($ip, ['93.170.143.0/24', '93.171.6.0/24', '10.0.101.101/20'])
)
{
    header('HTTP/1.0 403 Forbidden');
    exit('You are not allowed to access this file.');
}
echo '<pre>';

echo shell_exec('php ../app/console sonata:cache:flush-all');
echo shell_exec('php ../app/console doctrine:cache:clear-metadata');
echo shell_exec('php ../app/console doctrine:cache:clear-query');
echo shell_exec('php ../app/console doctrine:cache:clear-result');

chdir('./../app/cache/');
shell_exec('rm -rf prod');
shell_exec('rm -rf dev');

opcache_reset();

echo 'Cache cleared';

echo '</pre>';
